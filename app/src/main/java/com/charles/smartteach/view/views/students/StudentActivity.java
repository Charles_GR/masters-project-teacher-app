package com.charles.smartteach.view.views.students;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import com.charles.smartteach.R;
import com.charles.smartteach.view.templates.TabbedActivity;
import com.charles.smartteach.view.views.students.accounts.StudentAccountsActivity;
import com.charles.smartteach.view.views.students.courses.StudentCoursesActivity;

public class StudentActivity extends TabbedActivity implements OnClickListener
{

    private static StudentActivity instance;

    public static StudentActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_student);
        super.onCreate(savedInstanceState);
        instance = this;
        tvActionBarTitle.setText(R.string.students_title);
        tvActionBarCourse.setVisibility(View.GONE);
        createTab(getString(R.string.accounts_title), StudentAccountsActivity.class);
        createTab(getString(R.string.courses_title), StudentCoursesActivity.class);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        outState.putInt("currentTab", thTabbedView.getCurrentTab());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        thTabbedView.setCurrentTab(savedInstanceState.getInt("currentTab"));
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onUpButtonPressed()
    {
        switch(thTabbedView.getCurrentTab())
        {
            case 0:
                StudentAccountsActivity.getInstance().onUpButtonPressed();
                break;
            case 1:
                StudentCoursesActivity.getInstance().onUpButtonPressed();
                break;
        }
    }

}