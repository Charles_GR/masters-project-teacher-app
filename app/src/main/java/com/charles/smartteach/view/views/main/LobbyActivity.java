package com.charles.smartteach.view.views.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.charles.smartteach.R;
import com.charles.smartteach.model.TeachApplication;
import com.charles.smartteach.view.templates.AppMainActivity;
import com.charles.smartteach.view.views.account.AccountActivity;
import com.charles.smartteach.view.views.classroom.ClassroomActivity;
import com.charles.smartteach.view.views.performance.PerformanceActivity;
import com.charles.smartteach.view.views.revision.RevisionActivity;
import com.charles.smartteach.view.views.students.StudentActivity;

public class LobbyActivity extends AppMainActivity implements OnClickListener
{

    private LinearLayout llAccount;
    private LinearLayout llStudents;
    private LinearLayout llClassroom;
    private LinearLayout llRevision;
    private LinearLayout llPerformance;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lobby);
        llAccount = (LinearLayout)findViewById(R.id.llAccount);
        llStudents = (LinearLayout)findViewById(R.id.llStudents);
        llClassroom = (LinearLayout)findViewById(R.id.llClassroom);
        llRevision = (LinearLayout)findViewById(R.id.llRevision);
        llPerformance = (LinearLayout)findViewById(R.id.llPerformance);
        llAccount.setOnClickListener(this);
        llStudents.setOnClickListener(this);
        llClassroom.setOnClickListener(this);
        llRevision.setOnClickListener(this);
        llPerformance.setOnClickListener(this);
        tvActionBarTitle.setText(R.string.lobby_title);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.llAccount:
                startActivity(new Intent(this, AccountActivity.class));
                break;
            case R.id.llStudents:
                startActivity(new Intent(this, StudentActivity.class));
                break;
            case R.id.llClassroom:
                if(courseIsSelected())
                {
                    startActivity(new Intent(this, ClassroomActivity.class));
                }
                break;
            case R.id.llRevision:
                if(courseIsSelected())
                {
                    startActivity(new Intent(this, RevisionActivity.class));
                }
                break;
            case R.id.llPerformance:
                startActivity(new Intent(this, PerformanceActivity.class));
                break;
            default:
                super.onClick(v);
                break;
        }
    }

    private boolean courseIsSelected()
    {
        if(TeachApplication.getInstance().getSelectedCourse() == null)
        {
            Toast.makeText(this, R.string.must_select_course, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

}