package com.charles.smartteach.view.views.classroom.questions;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
import com.charles.smartteach.R;
import com.charles.smartteach.model.database.classroom.questions.Question;
import com.charles.smartteach.model.database.classroom.questions.ShortAnswerQuestion;
import com.charles.smartteach.processing.database.ClassroomData;
import com.charles.smartteach.processing.database.FirebaseData;
import com.charles.smartteach.view.extras.DataCharts;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import java.util.HashMap;
import java.util.List;

public class ClassroomQuestionResultsView implements OnClickListener, OnItemSelectedListener, OnCheckedChangeListener
{

    private ClassroomQuestionsActivity parent;
    private BarChart bcResults;
    private Button btnDeleteShortAnswerResponses;
    private Button btnDeleteCurrentQuestion;
    private Button btnViewChart;
    private LinearLayout llDeleteShortAnswerResponses;
    private PieChart pcResults;
    private Spinner spnShortAnswer;
    private Spinner spnStudent;
    private Switch swChartType;
    private TextView tvQuestion;
    private TextView tvResults;
    private TextView tvStudentAnswers;
    private ViewFlipper vfDataCharts;

    public ClassroomQuestionResultsView(ClassroomQuestionsActivity parent)
    {
        this.parent = parent;
        bcResults = (BarChart)parent.findViewById(R.id.bcResults);
        btnDeleteShortAnswerResponses = (Button)parent.findViewById(R.id.btnDeleteShortAnswerResponses);
        btnDeleteCurrentQuestion = (Button)parent.findViewById(R.id.btnDeleteCurrentQuestion);
        btnViewChart = (Button)parent.findViewById(R.id.btnViewChart);
        llDeleteShortAnswerResponses = (LinearLayout)parent.findViewById(R.id.llDeleteShortAnswerResponses);
        pcResults = (PieChart)parent.findViewById(R.id.pcResults);
        spnShortAnswer= (Spinner)parent.findViewById(R.id.spnShortAnswer);
        spnStudent = (Spinner)parent.findViewById(R.id.spnStudent);
        swChartType = (Switch)parent.findViewById(R.id.swChartType);
        tvQuestion = (TextView)parent.findViewById(R.id.tvQuestionB);
        tvResults = (TextView)parent.findViewById(R.id.tvResults);
        tvStudentAnswers = (TextView)parent.findViewById(R.id.tvStudentAnswersValue);
        vfDataCharts = (ViewFlipper)parent.findViewById(R.id.vfDataCharts);
        btnDeleteShortAnswerResponses.setOnClickListener(this);
        btnDeleteCurrentQuestion.setOnClickListener(this);
        btnViewChart.setOnClickListener(this);
        spnStudent.setOnItemSelectedListener(this);
        swChartType.setOnCheckedChangeListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnDeleteShortAnswerResponses:
                deleteShortAnswerResponses();
                break;
            case R.id.btnDeleteCurrentQuestion:
                FirebaseData.getClassroomQuestionsRef().child(parent.question.getKey()).removeValue();
                parent.vfQuestions.setDisplayedChild(0);
                break;
            case R.id.btnViewChart:
                viewChart();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        tvStudentAnswers.setText(this.parent.question.getResponses().get(spnStudent.getSelectedItem().toString()).toString());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
    {
        vfDataCharts.setDisplayedChild(swChartType.isChecked() ? 1 : 0);
    }

    private void deleteShortAnswerResponses()
    {
        String selectedAnswer = (String)spnShortAnswer.getSelectedItem();
        if(selectedAnswer == null)
        {
            Toast.makeText(parent, R.string.no_answer_selected, Toast.LENGTH_SHORT).show();
        }
        else
        {
            ClassroomData.deleteShortAnswerResponses(parent.question, selectedAnswer);
        }
    }

    @SuppressWarnings("unchecked")
    private void viewChart()
    {
        HashMap<String, Integer> answerCounts = parent.question.calcAnswerCounts();
        if(answerCounts.isEmpty())
        {
            Toast.makeText(parent, R.string.no_chart_data_available, Toast.LENGTH_SHORT).show();
        }
        else
        {
            parent.vfQuestions.setDisplayedChild(4);
            DataCharts.drawBarChart(bcResults, answerCounts);
            DataCharts.drawPieChart(pcResults, answerCounts);
        }
    }

    protected void displayResults(List<Question> questions)
    {
        if(parent.question == null)
        {
            return;
        }
        for(Question question : questions)
        {
            if(question.getKey().equals(parent.question.getKey()))
            {
                parent.question = question;
                displayResults(question);
                break;
            }
        }
    }

    @SuppressWarnings("unchecked")
    protected void displayResults(Question question)
    {
        tvQuestion.setText(question.getQuestion());
        tvResults.setText(question.writeResults());
        tvStudentAnswers.setText("");
        spnStudent.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, question.getResponders()));
        if(question.getType().equals(ShortAnswerQuestion.QUESTION_TYPE_TAG))
        {
            ShortAnswerQuestion shortAnswerQuestion = (ShortAnswerQuestion)question;
            spnShortAnswer.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, shortAnswerQuestion.getShortAnswers()));
            llDeleteShortAnswerResponses.setVisibility(View.VISIBLE);
        }
        else
        {
            llDeleteShortAnswerResponses.setVisibility(View.GONE);
        }
        HashMap<String, Integer> answerCounts = question.calcAnswerCounts();
        DataCharts.drawBarChart(bcResults, answerCounts);
        DataCharts.drawPieChart(pcResults, answerCounts);
    }

}
