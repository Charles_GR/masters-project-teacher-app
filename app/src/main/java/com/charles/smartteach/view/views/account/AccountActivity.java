package com.charles.smartteach.view.views.account;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.smartteach.R;
import com.charles.smartteach.model.runnable.SingleArgRunnable;
import com.charles.smartteach.processing.database.AccountData;
import com.charles.smartteach.processing.database.LoginData;
import com.charles.smartteach.view.templates.AppNavActivity;
import com.firebase.client.FirebaseError;

public class AccountActivity extends AppNavActivity implements OnClickListener
{

    private Button btnSaveUsernameAndEmail;
    private Button btnSavePassword;
    private EditText etCurrentPassword;
    private EditText etUsername;
    private EditText etEmail;
    private EditText etNewPassword;
    private EditText etConfirmNewPassword;
    private ProgressBar pbProgress;
    private TextView tvSuccessMessage;
    private TextView tvErrorMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        btnSaveUsernameAndEmail = (Button)findViewById(R.id.btnSaveUsernameAndEmail);
        btnSavePassword = (Button)findViewById(R.id.btnSavePassword);
        etCurrentPassword = (EditText)findViewById(R.id.etCurrentPassword);
        etUsername = (EditText)findViewById(R.id.etUsername);
        etEmail = (EditText)findViewById(R.id.etEmail);
        etNewPassword = (EditText)findViewById(R.id.etNewPassword);
        etConfirmNewPassword = (EditText)findViewById(R.id.etConfirmNewPassword);
        pbProgress = (ProgressBar)findViewById(R.id.pbProgress);
        tvSuccessMessage = (TextView)findViewById(R.id.tvSuccessMessage);
        tvErrorMessage = (TextView)findViewById(R.id.tvErrorMessage);
        btnSaveUsernameAndEmail.setOnClickListener(this);
        btnSavePassword.setOnClickListener(this);
        etUsername.setText(LoginData.getUsername());
        etEmail.setText(LoginData.getEmail());
        tvActionBarTitle.setText(R.string.account_title);
        tvActionBarCourse.setVisibility(View.GONE);
    }

    @Override
    protected void onUpButtonPressed()
    {
        goToLobby();
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnSaveUsernameAndEmail:
                changeUsernameAndEmail();
                break;
            case R.id.btnSavePassword:
                changePassword();
                break;
            default:
                super.onClick(v);
                break;
        }
    }

    private void changeUsernameAndEmail()
    {
        clearMessages();
        if(!validChangeUsernameAndEmailInputs())
        {
            return;
        }
        setActionInProgress();
        AccountData.changeUsernameAndEmail(etUsername.getText().toString(), LoginData.getEmail(), etCurrentPassword.getText().toString(),
                etEmail.getText().toString(), changeUsernameAndEmailOnSuccess, changeUsernameAndEmailOnError);
    }

    private void changePassword()
    {
        clearMessages();
        if(!validChangePasswordInputs())
        {
            return;
        }
        setActionInProgress();
        AccountData.changePassword(etEmail.getText().toString(), etCurrentPassword.getText().toString(),
                etNewPassword.getText().toString(), changePasswordOnSuccess, changePasswordOnError);
    }

    private boolean validChangeUsernameAndEmailInputs()
    {
        if(etCurrentPassword.getText().toString().isEmpty())
        {
            etCurrentPassword.requestFocus();
            etCurrentPassword.setError(getString(R.string.current_password_empty));
            return false;
        }
        if(etUsername.getText().toString().isEmpty())
        {
            etUsername.requestFocus();
            etUsername.setError(getString(R.string.username_empty));
            return false;
        }
        if(etEmail.getText().toString().isEmpty())
        {
            etEmail.requestFocus();
            etEmail.setError(getString(R.string.email_empty));
            return false;
        }
        if(etUsername.getText().toString().equals(LoginData.getUsername()) &&
                etEmail.getText().toString().equals(LoginData.getEmail()))
        {
            etUsername.requestFocus();
            etUsername.setError(getString(R.string.username_email_unchanged));
            etEmail.setError(getString(R.string.username_email_unchanged));
            return false;
        }
        return true;
    }

    private boolean validChangePasswordInputs()
    {
        if(etCurrentPassword.getText().toString().isEmpty())
        {
            etCurrentPassword.requestFocus();
            etCurrentPassword.setError(getString(R.string.current_password_empty));
            return false;
        }
        if(etNewPassword.getText().toString().isEmpty())
        {
            etNewPassword.requestFocus();
            etNewPassword.setError(getString(R.string.new_password_empty));
            return false;
        }
        if(etConfirmNewPassword.getText().toString().isEmpty())
        {
            etConfirmNewPassword.requestFocus();
            etConfirmNewPassword.setError(getString(R.string.confirm_new_password_empty));
            return false;
        }
        if(!etNewPassword.getText().toString().equals(etConfirmNewPassword.getText().toString()))
        {
            etNewPassword.requestFocus();
            etNewPassword.setError(getString(R.string.new_password_not_match));
            return false;
        }
        if(etCurrentPassword.getText().toString().equals(etNewPassword.getText().toString()))
        {
            etNewPassword.requestFocus();
            etNewPassword.setError(getString(R.string.new_password_same_as_current));
            etConfirmNewPassword.setError(getString(R.string.new_password_same_as_current));
            return false;
        }
        if(etNewPassword.getText().toString().length() < 6)
        {
            etNewPassword.requestFocus();
            etNewPassword.setError(getString(R.string.new_password_too_short));
            return false;
        }
        return true;
    }

    private void setActionInProgress()
    {
        hideKeyboard();
        clearMessages();
        btnSaveUsernameAndEmail.setEnabled(false);
        btnSavePassword.setEnabled(false);
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void setActionComplete()
    {
        btnSaveUsernameAndEmail.setEnabled(true);
        btnSavePassword.setEnabled(true);
        pbProgress.setVisibility(View.GONE);
    }

    private void clearMessages()
    {
        etCurrentPassword.setError(null);
        etUsername.setError(null);
        etEmail.setError(null);
        etNewPassword.setError(null);
        etConfirmNewPassword.setError(null);
        tvErrorMessage.setText("");
        tvSuccessMessage.setText("");
    }

    private Runnable changeUsernameAndEmailOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            tvSuccessMessage.setText(R.string.username_email_changed);
            setActionComplete();
        }
    };

    private SingleArgRunnable<FirebaseError> changeUsernameAndEmailOnError = new SingleArgRunnable<FirebaseError>()
    {
        @Override
        public void run(FirebaseError firebaseError)
        {
            if(firebaseError == null)
            {
                etUsername.requestFocus();
                etUsername.setError(getString(R.string.username_already_taken));
                setActionComplete();
            }
            else if(firebaseError.getCode() == FirebaseError.EMAIL_TAKEN && etEmail.getText().toString().equals(LoginData.getEmail()))
            {
                AccountData.updateUsernameEmailLinks(etUsername.getText().toString(), etEmail.getText().toString(),
                        changeUsernameAndEmailOnSuccess, changeUsernameAndEmailOnError);
            }
            else
            {
                tvErrorMessage.setText(firebaseError.getMessage());
                setActionComplete();
            }
        }
    };

    private Runnable changePasswordOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            etCurrentPassword.setText("");
            etNewPassword.setText("");
            etConfirmNewPassword.setText("");
            tvSuccessMessage.setText(R.string.password_changed);
            setActionComplete();
        }
    };

    private SingleArgRunnable<String> changePasswordOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String message)
        {
            tvErrorMessage.setText(message);
            setActionComplete();
        }
    };

}