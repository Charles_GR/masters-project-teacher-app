package com.charles.smartteach.view.views.revision.quizzes;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.smartteach.R;
import com.charles.smartteach.model.database.revision.quizzes.*;

public class RevisionAnswerFormView implements OnClickListener
{

    private RevisionQuizzesActivity parent;
    private Button btnAddAnswer;
    private Button btnSaveAnswer;
    private EditText etAnswer;
    private CheckBox chkCorrectAnswer;
    private EditText etFeedbackForSelect;
    private EditText etFeedbackForNotSelect;
    private LinearLayout llFeedbackForNotSelect;
    private ViewFlipper vfAnswerActionButton;

    public RevisionAnswerFormView(RevisionQuizzesActivity parent)
    {
        this.parent = parent;
        btnAddAnswer = (Button)parent.findViewById(R.id.btnAddAnswer);
        btnSaveAnswer = (Button)parent.findViewById(R.id.btnSaveAnswer);
        etAnswer = (EditText)parent.findViewById(R.id.etAnswer);
        etFeedbackForSelect = (EditText)parent.findViewById(R.id.etFeedbackForSelect);
        etFeedbackForNotSelect = (EditText)parent.findViewById(R.id.etFeedbackForNotSelect);
        chkCorrectAnswer = (CheckBox)parent.findViewById(R.id.chkCorrectAnswer);
        llFeedbackForNotSelect = (LinearLayout)parent.findViewById(R.id.llFeedbackForNotSelect);
        vfAnswerActionButton = (ViewFlipper)parent.findViewById(R.id.vfAnswerActionButton);
        btnAddAnswer.setOnClickListener(this);
        btnSaveAnswer.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnAddAnswer:
                addAnswer();
                break;
            case R.id.btnSaveAnswer:
                saveAnswer();
                break;
        }
    }

    protected void createAnswer()
    {
        etAnswer.setError(null);
        etFeedbackForSelect.setError(null);
        etFeedbackForNotSelect.setError(null);
        etAnswer.setText("");
        chkCorrectAnswer.setChecked(false);
        etFeedbackForSelect.setText("");
        etFeedbackForNotSelect.setText("");
        llFeedbackForNotSelect.setVisibility(parent.question.getType().equals(MultiSelectQuestion.QUESTION_TYPE_TAG) ? View.VISIBLE : View.GONE);
        vfAnswerActionButton.setDisplayedChild(0);
    }

    protected void editAnswer()
    {
        etAnswer.setError(null);
        etFeedbackForSelect.setError(null);
        etFeedbackForNotSelect.setError(null);
        etAnswer.setText(parent.answer.getAnswer());
        chkCorrectAnswer.setChecked(parent.answer.getCorrect());
        vfAnswerActionButton.setDisplayedChild(1);
        if(parent.answer instanceof SingleSelectAnswer)
        {
            SingleSelectAnswer singleSelectAnswer = (SingleSelectAnswer)parent.answer;
            etFeedbackForNotSelect.setVisibility(View.GONE);
            etFeedbackForSelect.setText(singleSelectAnswer.getFeedback());
        }
        else if(parent.answer instanceof MultiSelectAnswer)
        {
            MultiSelectAnswer multiSelectAnswer = (MultiSelectAnswer)parent.answer;
            etFeedbackForNotSelect.setVisibility(View.VISIBLE);
            etFeedbackForSelect.setText(multiSelectAnswer.getFeedbackForSelect());
            etFeedbackForNotSelect.setText(multiSelectAnswer.getFeedbackForNotSelect());
        }
    }

    private void addAnswer()
    {
        etAnswer.setError(null);
        parent.hideKeyboard();
        if(!addOrEditAnswerValidInputs())
        {
            return;
        }
        SelectQuestion selectQuestion = (SelectQuestion)parent.question;
        if(!selectQuestion.addAnswer(etAnswer.getText().toString(), chkCorrectAnswer.isChecked(), etFeedbackForSelect.getText().toString(), etFeedbackForNotSelect.getText().toString()))
        {
            etAnswer.requestFocus();
            etAnswer.setError(parent.getString(R.string.answer_already_added));
            return;
        }
        parent.revisionQuestionFormView.updateAnswerList();
        parent.vfQuizzes.setDisplayedChild(2);
    }

    @SuppressWarnings("unchecked")
    private void saveAnswer()
    {
        etAnswer.setError(null);
        parent.hideKeyboard();
        if(!addOrEditAnswerValidInputs())
        {
            return;
        }
        parent.answer.setAnswer(etAnswer.getText().toString());
        parent.answer.setCorrect(chkCorrectAnswer.isChecked());
        parent.answer.putFeedbackFromArray(etFeedbackForSelect.getText().toString(), etFeedbackForNotSelect.getText().toString());
        etAnswer.setText("");
        chkCorrectAnswer.setChecked(false);
        parent.revisionQuestionFormView.updateAnswerList();
        parent.vfQuizzes.setDisplayedChild(2);
    }

    private boolean addOrEditAnswerValidInputs()
    {
        if(etAnswer.getText().toString().isEmpty())
        {
            etAnswer.requestFocus();
            etAnswer.setError(parent.getString(R.string.answer_empty));
            return false;
        }
        if(etFeedbackForSelect.getText().toString().isEmpty())
        {
            etFeedbackForSelect.requestFocus();
            etFeedbackForSelect.setError(parent.getString(R.string.feedback_needed));
            return false;
        }
        if(parent.question.getType().equals(MultiSelectQuestion.QUESTION_TYPE_TAG) && etFeedbackForNotSelect.getText().toString().isEmpty())
        {
            etFeedbackForNotSelect.requestFocus();
            etFeedbackForNotSelect.setError(parent.getString(R.string.feedback_needed));
            return false;
        }
        return true;
    }

}
