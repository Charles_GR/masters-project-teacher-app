package com.charles.smartteach.view.views.students.courses;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.smartteach.R;
import com.charles.smartteach.model.database.students.Course;
import com.charles.smartteach.view.templates.AppActivity;
import com.charles.smartteach.view.views.students.StudentActivity;

public class StudentCoursesActivity extends AppActivity implements OnClickListener
{

    private static StudentCoursesActivity instance;

    protected StudentCourseListView studentCourseListView;
    protected StudentCourseFormView studentCourseFormView;
    protected StudentCourseStudentListView studentCourseStudentListView;
    protected ViewFlipper vfCourses;

    protected Course course;
    protected String currentCourseCode;

    public static StudentCoursesActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        instance = this;
        setContentView(R.layout.activity_student_courses);
        studentCourseListView = new StudentCourseListView(this);
        studentCourseFormView = new StudentCourseFormView(this);
        studentCourseStudentListView = new StudentCourseStudentListView(this);
        vfCourses = (ViewFlipper)findViewById(R.id.vfCourses);
    }

    @Override
    public void onClick(View v)
    {
        StudentActivity.getInstance().onClick(v);
    }

    public void onUpButtonPressed()
    {
        switch(vfCourses.getDisplayedChild())
        {
            case 0:
                goToLobby();
                break;
            case 1:
                vfCourses.setDisplayedChild(0);
                break;
            case 2:
                vfCourses.setDisplayedChild(0);
                break;
        }
    }

}