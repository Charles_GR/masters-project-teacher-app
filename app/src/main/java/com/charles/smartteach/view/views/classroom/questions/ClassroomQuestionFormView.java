package com.charles.smartteach.view.views.classroom.questions;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import com.charles.smartteach.R;
import com.charles.smartteach.model.adapters.classroom.questions.AnswerAdapter;
import com.charles.smartteach.model.database.classroom.questions.*;
import com.charles.smartteach.processing.database.FirebaseData;
import com.firebase.client.Firebase;

public class ClassroomQuestionFormView implements OnClickListener, OnItemSelectedListener
{

    private ClassroomQuestionsActivity parent;
    private Button btnCreateAnswer;
    private Button btnEditAnswer;
    private Button btnDeleteAnswer;
    private Button btnAddAnswer;
    private Button btnSaveAnswer;
    private CheckBox chkCorrectAnswer;
    private Button btnPostQuestion;
    private EditText etQuestion;
    private EditText etAnswer;
    private EditText etCorrectAnswer;
    private ListView lvAnswers;
    private Spinner spnAnswerType;
    private Switch swCorrectAnswer;
    private TextView tvAnswers;
    private ViewFlipper vfAnswers;
    private ViewFlipper vfAnswerActionButton;

    private Answer answer;

    public ClassroomQuestionFormView(ClassroomQuestionsActivity parent)
    {
        this.parent = parent;
        btnCreateAnswer = (Button)parent.findViewById(R.id.btnCreateAnswer);
        btnEditAnswer = (Button)parent.findViewById(R.id.btnEditAnswer);
        btnDeleteAnswer = (Button)parent.findViewById(R.id.btnDeleteAnswer);
        btnAddAnswer = (Button)parent.findViewById(R.id.btnAddAnswer);
        btnSaveAnswer = (Button)parent.findViewById(R.id.btnSaveAnswer);
        btnPostQuestion = (Button)parent.findViewById(R.id.btnPostQuestion);
        chkCorrectAnswer = (CheckBox)parent.findViewById(R.id.chkCorrectAnswer);
        etQuestion = (EditText)parent.findViewById(R.id.etQuestion);
        etAnswer = (EditText)parent.findViewById(R.id.etAnswer);
        etCorrectAnswer = (EditText)parent.findViewById(R.id.etCorrectAnswer);
        lvAnswers = (ListView)parent.findViewById(R.id.lvAnswers);
        spnAnswerType = (Spinner)parent.findViewById(R.id.spnAnswerType);
        swCorrectAnswer = (Switch)parent.findViewById(R.id.swCorrectAnswer);
        tvAnswers = (TextView)parent.findViewById(R.id.tvAnswers);
        vfAnswers = (ViewFlipper)parent.findViewById(R.id.vfAnswers);
        vfAnswerActionButton = (ViewFlipper)parent.findViewById(R.id.vfAnswerActionButton);
        btnCreateAnswer.setOnClickListener(this);
        btnEditAnswer.setOnClickListener(this);
        btnAddAnswer.setOnClickListener(this);
        btnSaveAnswer.setOnClickListener(this);
        btnDeleteAnswer.setOnClickListener(this);
        btnPostQuestion.setOnClickListener(this);
        spnAnswerType.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{SingleSelectQuestion.QUESTION_TYPE_TAG,
                MultiSelectQuestion.QUESTION_TYPE_TAG, TrueOrFalseQuestion.QUESTION_TYPE_TAG, ShortAnswerQuestion.QUESTION_TYPE_TAG}));
        spnAnswerType.setOnItemSelectedListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnCreateAnswer:
                createAnswer();
                break;
            case R.id.btnEditAnswer:
                editAnswer();
                break;
            case R.id.btnAddAnswer:
                addAnswer();
                break;
            case R.id.btnSaveAnswer:
                saveAnswer();
                break;
            case R.id.btnDeleteAnswer:
                deleteAnswer();
                break;
            case R.id.btnPostQuestion:
                postQuestion();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        if(this.parent.question == null || !this.parent.question.getType().equals(spnAnswerType.getSelectedItem().toString()))
        {
            createQuestion(spnAnswerType.getSelectedItem().toString());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    protected void createQuestion()
    {
        etQuestion.setError(null);
        etQuestion.setText("");
        silentSetAnswerTypeSelection(0);
        createQuestion(spnAnswerType.getSelectedItem().toString());
    }

    private void createQuestion(String type)
    {
        switch(type)
        {
            case SingleSelectQuestion.QUESTION_TYPE_TAG:
                parent.question = new SingleSelectQuestion();
                vfAnswers.setDisplayedChild(0);
                lvAnswers.setAdapter(new AnswerAdapter(parent));
                tvAnswers.setError(null);
                break;
            case MultiSelectQuestion.QUESTION_TYPE_TAG:
                parent.question = new MultiSelectQuestion();
                vfAnswers.setDisplayedChild(0);
                lvAnswers.setAdapter(new AnswerAdapter(parent));
                tvAnswers.setError(null);
                break;
            case TrueOrFalseQuestion.QUESTION_TYPE_TAG:
                parent.question = new TrueOrFalseQuestion();
                vfAnswers.setDisplayedChild(1);
                swCorrectAnswer.setChecked(false);
                break;
            case ShortAnswerQuestion.QUESTION_TYPE_TAG:
                parent.question = new ShortAnswerQuestion();
                vfAnswers.setDisplayedChild(2);
                etCorrectAnswer.setError(null);
                etCorrectAnswer.setText("");
                break;
        }
    }

    protected void cloneQuestion()
    {
        etQuestion.setText(parent.question.getQuestion());
        switch(parent.question.getType())
        {
            case SingleSelectQuestion.QUESTION_TYPE_TAG:
                parent.question = new SingleSelectQuestion((SingleSelectQuestion)parent.question);
                SingleSelectQuestion newSingleSelectQuestion = (SingleSelectQuestion)parent.question;
                silentSetAnswerTypeSelection(0);
                vfAnswers.setDisplayedChild(0);
                lvAnswers.setAdapter(new AnswerAdapter(parent, newSingleSelectQuestion.getAnswers()));
                tvAnswers.setError(null);
                break;
            case MultiSelectQuestion.QUESTION_TYPE_TAG:
                parent.question = new MultiSelectQuestion((MultiSelectQuestion)parent.question);
                MultiSelectQuestion newMultiSelectQuestion = (MultiSelectQuestion)parent.question;
                silentSetAnswerTypeSelection(1);
                vfAnswers.setDisplayedChild(0);
                lvAnswers.setAdapter(new AnswerAdapter(parent, newMultiSelectQuestion.getAnswers()));
                tvAnswers.setError(null);
                break;
            case TrueOrFalseQuestion.QUESTION_TYPE_TAG:
                parent.question = new TrueOrFalseQuestion((TrueOrFalseQuestion)parent.question);
                TrueOrFalseQuestion newTrueOrFalseQuestion = (TrueOrFalseQuestion)parent.question;
                silentSetAnswerTypeSelection(2);
                vfAnswers.setDisplayedChild(1);
                swCorrectAnswer.setChecked(newTrueOrFalseQuestion.getCorrectAnswer());
                break;
            case ShortAnswerQuestion.QUESTION_TYPE_TAG:
                parent.question = new ShortAnswerQuestion((ShortAnswerQuestion)parent.question);
                ShortAnswerQuestion newShortAnswerQuestion = (ShortAnswerQuestion)parent.question;
                silentSetAnswerTypeSelection(3);
                vfAnswers.setDisplayedChild(2);
                etCorrectAnswer.setError(null);
                etCorrectAnswer.setText(newShortAnswerQuestion.getCorrectAnswer());
                break;
        }
    }

    private Answer getSelectedAnswer()
    {
        Answer selectedAnswer = (Answer)lvAnswers.getItemAtPosition(lvAnswers.getCheckedItemPosition());
        if(selectedAnswer == null)
        {
            tvAnswers.requestFocus();
            tvAnswers.setError(parent.getString(R.string.no_answer_selected));
        }
        return selectedAnswer;
    }


    @SuppressWarnings("unchecked")
    private void addAnswer()
    {
        etAnswer.setError(null);
        parent.hideKeyboard();
        if(!addOrEditAnswerValidInputs())
        {
            return;
        }
        SelectQuestion selectQuestion = (SelectQuestion)parent.question;
        if(!selectQuestion.addAnswer(etAnswer.getText().toString(), chkCorrectAnswer.isChecked()))
        {
            etAnswer.requestFocus();
            etAnswer.setError(parent.getString(R.string.answer_already_added));
            return;
        }
        parent.vfQuestions.setDisplayedChild(1);
        etAnswer.setText("");
        chkCorrectAnswer.setChecked(false);
        lvAnswers.setAdapter(new AnswerAdapter(parent, selectQuestion.getAnswers()));
    }

    private void createAnswer()
    {
        tvAnswers.setError(null);
        etAnswer.setError(null);
        SelectQuestion selectQuestion = (SelectQuestion)parent.question;
        if(selectQuestion.getAnswers().size() == parent.getResources().getInteger(R.integer.max_answers))
        {
            tvAnswers.requestFocus();
            tvAnswers.setError(parent.getString(R.string.max_answers_reached));
            return;
        }
        etAnswer.setText("");
        chkCorrectAnswer.setChecked(false);
        parent.vfQuestions.setDisplayedChild(2);
        vfAnswerActionButton.setDisplayedChild(0);
    }

    @SuppressWarnings("unchecked")
    private void editAnswer()
    {
        tvAnswers.setError(null);
        etAnswer.setError(null);
        answer = getSelectedAnswer();
        if(answer != null)
        {
            etAnswer.setText(answer.getAnswer());
            chkCorrectAnswer.setChecked(answer.getCorrect());
            vfAnswerActionButton.setDisplayedChild(1);
            parent.vfQuestions.setDisplayedChild(2);
        }
    }

    @SuppressWarnings("unchecked")
    private void deleteAnswer()
    {
        tvAnswers.setError(null);
        SelectQuestion selectQuestion = (SelectQuestion)parent.question;
        Answer selectedAnswer = getSelectedAnswer();
        if(selectedAnswer == null)
        {
            return;
        }
        selectQuestion.removeAnswer(selectedAnswer.getAnswer());
        lvAnswers.setAdapter(new AnswerAdapter(parent, selectQuestion.getAnswers()));
    }

    @SuppressWarnings("unchecked")
    private void saveAnswer()
    {
        etAnswer.setError(null);
        parent.hideKeyboard();
        if(!addOrEditAnswerValidInputs())
        {
            return;
        }
        SelectQuestion selectQuestion = (SelectQuestion)parent.question;
        answer.setAnswer(etAnswer.getText().toString());
        answer.setCorrect(chkCorrectAnswer.isChecked());
        parent.vfQuestions.setDisplayedChild(1);
        etAnswer.setText("");
        chkCorrectAnswer.setChecked(false);
        lvAnswers.setAdapter(new AnswerAdapter(parent, selectQuestion.getAnswers()));
    }

    private void postQuestion()
    {
        tvAnswers.setError(null);
        parent.question.setQuestion(etQuestion.getText().toString());
        if(!postQuestionValidInputs())
        {
            return;
        }
        switch(parent.question.getType())
        {
            case SingleSelectQuestion.QUESTION_TYPE_TAG:
            case MultiSelectQuestion.QUESTION_TYPE_TAG:
                SelectQuestion selectQuestion = (SelectQuestion)parent.question;
                String errorMessage = selectQuestion.checkAnswers(parent);
                if(errorMessage != null)
                {
                    tvAnswers.requestFocus();
                    tvAnswers.setError(errorMessage);
                    return;
                }
                lvAnswers.setAdapter(new AnswerAdapter(parent));
                break;
            case TrueOrFalseQuestion.QUESTION_TYPE_TAG:
                TrueOrFalseQuestion trueOrFalseQuestion = (TrueOrFalseQuestion)parent.question;
                trueOrFalseQuestion.setCorrectAnswer(swCorrectAnswer.isChecked());
                swCorrectAnswer.setChecked(false);
                break;
            case ShortAnswerQuestion.QUESTION_TYPE_TAG:
                if(etCorrectAnswer.getText().toString().isEmpty())
                {
                    etCorrectAnswer.requestFocus();
                    etCorrectAnswer.setError(parent.getString(R.string.correct_answer_empty));
                    return;
                }
                ShortAnswerQuestion shortAnswerQuestion = (ShortAnswerQuestion)parent.question;
                shortAnswerQuestion.setCorrectAnswer(etCorrectAnswer.getText().toString());
                etCorrectAnswer.setText("");
                break;
        }
        Firebase newQuestionRef = FirebaseData.getClassroomQuestionsRef().push();
        parent.question.setKey(newQuestionRef.getKey());
        newQuestionRef.setValue(parent.question);
        etQuestion.setText("");
        spnAnswerType.setSelection(0);
        parent.vfQuestions.setDisplayedChild(0);
    }

    private void silentSetAnswerTypeSelection(int position)
    {
        spnAnswerType.setOnItemSelectedListener(null);
        spnAnswerType.setSelection(position);
        spnAnswerType.setOnItemSelectedListener(this);
    }

    private boolean postQuestionValidInputs()
    {
        if(etQuestion.getText().toString().isEmpty())
        {
            etAnswer.requestFocus();
            etQuestion.setError(parent.getString(R.string.question_empty));
            return false;
        }
        return true;
    }

    private boolean addOrEditAnswerValidInputs()
    {
        if(etAnswer.getText().toString().isEmpty())
        {
            etAnswer.requestFocus();
            etAnswer.setError(parent.getString(R.string.answer_empty));
            return false;
        }
        return true;
    }

}
