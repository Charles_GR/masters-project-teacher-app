package com.charles.smartteach.view.views.students.courses;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import com.charles.smartteach.R;
import com.charles.smartteach.model.adapters.students.CourseAdapter;
import com.charles.smartteach.model.database.students.Course;
import com.charles.smartteach.model.runnable.ListRunnable;
import com.charles.smartteach.model.runnable.SingleArgRunnable;
import com.charles.smartteach.processing.database.CourseData;
import com.charles.smartteach.processing.database.FirebaseData;
import com.firebase.client.FirebaseError;

import java.util.List;

public class StudentCourseListView implements OnClickListener, OnItemSelectedListener, TextWatcher
{

    private StudentCoursesActivity parent;
    private Button btnCreateCourse;
    private Button btnEditCourseInfo;
    private Button btnViewCourseStudents;
    private Button btnDeleteCourse;
    private EditText etCourseSearchQuery;
    private ListView lvCourses;
    private ProgressBar pbCourseList;
    private Spinner spnCourseSortField;
    private Spinner spnCourseSortOrdering;
    private Spinner spnCourseSearchField;
    private TextView tvCourses;
    private TextView tvNoCoursesYet;
    private TextView tvCourseListErrorMessage;

    public StudentCourseListView(StudentCoursesActivity parent)
    {
        this.parent = parent;
        btnCreateCourse = (Button)parent.findViewById(R.id.btnCreateCourse);
        btnEditCourseInfo = (Button)parent.findViewById(R.id.btnEditCourseInfo);
        btnViewCourseStudents = (Button)parent.findViewById(R.id.btnViewCourseStudents);
        btnDeleteCourse = (Button)parent.findViewById(R.id.btnDeleteCourse);
        etCourseSearchQuery = (EditText)parent.findViewById(R.id.etCourseSearchQuery);
        lvCourses = (ListView)parent.findViewById(R.id.lvCourses);
        pbCourseList = (ProgressBar)parent.findViewById(R.id.pbCourseList);
        spnCourseSortField = (Spinner)parent.findViewById(R.id.spnCourseSortField);
        spnCourseSortOrdering = (Spinner)parent.findViewById(R.id.spnCourseSortOrdering);
        spnCourseSearchField = (Spinner)parent.findViewById(R.id.spnCourseSearchField);
        tvCourses = (TextView)parent.findViewById(R.id.tvCourses);
        tvNoCoursesYet = (TextView)parent.findViewById(R.id.tvNoCoursesYet);
        tvCourseListErrorMessage = (TextView)parent.findViewById(R.id.tvCourseListError);
        btnCreateCourse.setOnClickListener(this);
        btnEditCourseInfo.setOnClickListener(this);
        btnViewCourseStudents.setOnClickListener(this);
        btnDeleteCourse.setOnClickListener(this);
        etCourseSearchQuery.addTextChangedListener(this);
        spnCourseSortField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Code", "Title", "# Students"}));
        spnCourseSortField.setOnItemSelectedListener(this);
        spnCourseSortOrdering.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Ascending", "Descending"}));
        spnCourseSortOrdering.setOnItemSelectedListener(this);
        spnCourseSearchField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Code", "Title"}));
        spnCourseSearchField.setOnItemSelectedListener(this);
        CourseData.getTeacherCourses(getCoursesOnSuccess, getCoursesOnError);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnCreateCourse:
                createCourse();
                break;
            case R.id.btnEditCourseInfo:
                editCourseInfo();
                break;
            case R.id.btnViewCourseStudents:
                viewCourseStudents();
                break;
            case R.id.btnDeleteCourse:
                deleteCourse();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        searchAndSortCourses();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        searchAndSortCourses();
    }

    @Override
    public void afterTextChanged(Editable s)
    {

    }

    private void searchAndSortCourses()
    {
        lvCourses.clearChoices();
        CourseAdapter courseAdapter = (CourseAdapter)lvCourses.getAdapter();
        if(courseAdapter != null)
        {
            courseAdapter.searchItems(spnCourseSearchField.getSelectedItem().toString(), etCourseSearchQuery.getText().toString());
            courseAdapter.sortItems(spnCourseSortField.getSelectedItem().toString(), spnCourseSortOrdering.getSelectedItem().toString());
        }
    }

    private void createCourse()
    {
        tvCourses.setError(null);
        parent.studentCourseFormView.createAccount();
        parent.vfCourses.setDisplayedChild(1);
    }

    private void editCourseInfo()
    {
        tvCourses.setError(null);
        parent.studentCourseFormView.clearCourseFormErrors();
        parent.course = (Course)lvCourses.getItemAtPosition(lvCourses.getCheckedItemPosition());
        if(parent.course == null)
        {
            tvCourses.requestFocus();
            tvCourses.setError(parent.getString(R.string.no_course_selected));
        }
        else
        {
            parent.studentCourseFormView.editCourseInfo();
            parent.vfCourses.setDisplayedChild(1);
        }
    }

    private void viewCourseStudents()
    {
        tvCourses.setError(null);
        parent.course = (Course)lvCourses.getItemAtPosition(lvCourses.getCheckedItemPosition());
        if(parent.course == null)
        {
            tvCourses.requestFocus();
            tvCourses.setError(parent.getString(R.string.no_course_selected));
        }
        else
        {
            parent.vfCourses.setDisplayedChild(2);
            parent.studentCourseStudentListView.getCourseStudents();
        }
    }

    private void deleteCourse()
    {
        tvCourses.setError(null);
        Course selectedCourse = (Course)lvCourses.getItemAtPosition(lvCourses.getCheckedItemPosition());
        if(selectedCourse == null)
        {
            tvCourses.requestFocus();
            tvCourses.setError(parent.getString(R.string.no_course_selected));
            return;
        }
        FirebaseData.getCourseRef(selectedCourse.getCode()).removeValue();
    }

    private ListRunnable<Course> getCoursesOnSuccess = new ListRunnable<Course>()
    {
        @Override
        public void run(List<Course> courses)
        {
            lvCourses.setAdapter(new CourseAdapter(parent, courses));
            tvNoCoursesYet.setVisibility(courses.isEmpty() ? View.VISIBLE : View.GONE);
            tvCourseListErrorMessage.setVisibility(View.GONE);
            pbCourseList.setVisibility(View.GONE);
            btnCreateCourse.setEnabled(true);
            btnEditCourseInfo.setEnabled(true);
            btnViewCourseStudents.setEnabled(true);
            btnDeleteCourse.setEnabled(true);
            searchAndSortCourses();
        }
    };

    private SingleArgRunnable<FirebaseError> getCoursesOnError = new SingleArgRunnable<FirebaseError>()
    {
        @Override
        public void run(FirebaseError firebaseError)
        {
            lvCourses.setAdapter(new CourseAdapter(parent));
            tvCourses.setVisibility(View.GONE);
            tvNoCoursesYet.setVisibility(View.GONE);
            tvCourseListErrorMessage.setText(firebaseError.getMessage());
            tvCourseListErrorMessage.setVisibility(View.VISIBLE);
            pbCourseList.setVisibility(View.GONE);
            btnCreateCourse.setEnabled(true);
            btnEditCourseInfo.setEnabled(true);
            btnViewCourseStudents.setEnabled(true);
            btnDeleteCourse.setEnabled(true);
        }
    };

}
