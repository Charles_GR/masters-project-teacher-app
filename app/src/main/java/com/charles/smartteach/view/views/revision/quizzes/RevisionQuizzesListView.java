package com.charles.smartteach.view.views.revision.quizzes;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import com.charles.smartteach.R;
import com.charles.smartteach.model.adapters.revision.quizzes.QuizAdapter;
import com.charles.smartteach.model.database.revision.quizzes.*;
import com.charles.smartteach.model.runnable.ListRunnable;
import com.charles.smartteach.model.runnable.SingleArgRunnable;
import com.charles.smartteach.processing.database.RevisionData;
import java.util.List;

public class RevisionQuizzesListView implements OnClickListener, OnItemSelectedListener, TextWatcher
{

    private RevisionQuizzesActivity parent;
    private Button btnCreateQuiz;
    private Button btnViewQuiz;
    private Button btnDeleteQuiz;
    private EditText etSearchQuery;
    private ListView lvQuizzes;
    private ProgressBar pbLoadQuizzes;
    private Spinner spnSortField;
    private Spinner spnSortOrdering;
    private Spinner spnSearchField;
    private TextView tvQuizzes;
    private TextView tvNoQuizzesYet;
    private TextView tvQuizListErrorMessage;

    public RevisionQuizzesListView(RevisionQuizzesActivity parent)
    {
        this.parent = parent;
        btnCreateQuiz = (Button)parent.findViewById(R.id.btnCreateQuiz);
        btnViewQuiz = (Button)parent.findViewById(R.id.btnViewQuiz);
        btnDeleteQuiz = (Button)parent.findViewById(R.id.btnDeleteQuiz);
        etSearchQuery = (EditText)parent.findViewById(R.id.etSearchQuery);
        lvQuizzes = (ListView)parent.findViewById(R.id.lvQuizzes);
        pbLoadQuizzes = (ProgressBar)parent.findViewById(R.id.pbLoadQuizzes);
        spnSortField = (Spinner)parent.findViewById(R.id.spnSortField);
        spnSortOrdering = (Spinner)parent.findViewById(R.id.spnSortOrdering);
        spnSearchField = (Spinner)parent.findViewById(R.id.spnSearchField);
        tvQuizzes = (TextView)parent.findViewById(R.id.tvQuizzes);
        tvNoQuizzesYet = (TextView)parent.findViewById(R.id.tvNoQuizzesYet);
        tvQuizListErrorMessage = (TextView)parent.findViewById(R.id.tvQuizListErrorMessage);
        btnCreateQuiz.setOnClickListener(this);
        btnViewQuiz.setOnClickListener(this);
        btnDeleteQuiz.setOnClickListener(this);
        etSearchQuery.addTextChangedListener(this);
        spnSortField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Title", "Date Created", "# Responses"}));
        spnSortField.setOnItemSelectedListener(this);
        spnSortOrdering.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Ascending", "Descending"}));
        spnSortOrdering.setOnItemSelectedListener(this);
        spnSearchField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Title"}));
        spnSearchField.setOnItemSelectedListener(this);
        RevisionData.getQuizzes(getQuizzesOnSuccess, getQuizzesOnError);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnCreateQuiz:
                createQuiz();
                break;
            case R.id.btnViewQuiz:
                viewQuiz();
                break;
            case R.id.btnDeleteQuiz:
                deleteQuiz();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        searchAndSortQuizzes();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        searchAndSortQuizzes();
    }

    @Override
    public void afterTextChanged(Editable s)
    {

    }

    private void searchAndSortQuizzes()
    {
        lvQuizzes.clearChoices();
        QuizAdapter quizAdapter = (QuizAdapter)lvQuizzes.getAdapter();
        if(quizAdapter != null)
        {
            quizAdapter.searchItems(spnSearchField.getSelectedItem().toString(), etSearchQuery.getText().toString());
            quizAdapter.sortItems(spnSortField.getSelectedItem().toString(), spnSortOrdering.getSelectedItem().toString());
        }
    }

    private Quiz getSelectedQuiz()
    {
        Quiz selectedQuiz = (Quiz)lvQuizzes.getItemAtPosition(lvQuizzes.getCheckedItemPosition());
        if(selectedQuiz == null)
        {
            tvQuizzes.requestFocus();
            tvQuizzes.setError(parent.getString(R.string.no_quiz_selected));
        }
        return selectedQuiz;
    }

    private void createQuiz()
    {
        tvQuizzes.setError(null);
        parent.revisionQuizFormView.clearQuizForm();
        parent.vfQuizzes.setDisplayedChild(1);
    }

    private void viewQuiz()
    {
        tvQuizzes.setError(null);
        parent.quiz = getSelectedQuiz();
        if(parent.quiz == null)
        {
            return;
        }
        parent.vfQuizzes.setDisplayedChild(5);
        parent.revisionQuizResultsView.displayResults();
    }

    private void deleteQuiz()
    {
        tvQuizzes.setError(null);
        Quiz quiz = getSelectedQuiz();
        if(quiz != null)
        {
            RevisionData.deleteQuiz(quiz.getKey(), deleteQuizOnError);
        }
    }

    private ListRunnable<Quiz> getQuizzesOnSuccess = new ListRunnable<Quiz>()
    {
        @Override
        public void run(List<Quiz> quizzes)
        {
            lvQuizzes.setAdapter(new QuizAdapter(parent, quizzes));
            tvNoQuizzesYet.setVisibility(quizzes.isEmpty() ? View.VISIBLE : View.GONE);
            tvQuizListErrorMessage.setVisibility(View.GONE);
            pbLoadQuizzes.setVisibility(View.GONE);
            btnCreateQuiz.setEnabled(true);
            btnViewQuiz.setEnabled(true);
            btnDeleteQuiz.setEnabled(true);
            parent.revisionQuizResultsView.displayResults(quizzes);
            searchAndSortQuizzes();
        }
    };

    private SingleArgRunnable<String> getQuizzesOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            lvQuizzes.setAdapter(new QuizAdapter(parent));
            tvNoQuizzesYet.setVisibility(View.GONE);
            tvQuizListErrorMessage.setText(text);
            tvQuizListErrorMessage.setVisibility(View.VISIBLE);
            pbLoadQuizzes.setVisibility(View.GONE);
            btnCreateQuiz.setEnabled(true);
            btnViewQuiz.setEnabled(true);
            btnDeleteQuiz.setEnabled(true);
        }
    };

    private SingleArgRunnable<String> deleteQuizOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            tvQuizListErrorMessage.setText(text);
            tvQuizListErrorMessage.setVisibility(View.VISIBLE);
        }
    };

}
