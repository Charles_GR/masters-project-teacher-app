package com.charles.smartteach.view.views.revision.quizzes;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.smartteach.R;
import com.charles.smartteach.model.adapters.revision.quizzes.QuestionAdapter;
import com.charles.smartteach.model.database.revision.quizzes.Question;
import com.charles.smartteach.model.database.revision.quizzes.Quiz;
import com.charles.smartteach.model.runnable.SingleArgRunnable;
import com.charles.smartteach.processing.database.RevisionData;

public class RevisionQuizFormView implements OnClickListener
{

    private RevisionQuizzesActivity parent;
    private Button btnCreateQuestion;
    private Button btnEditQuestion;
    private Button btnCloneQuestion;
    private Button btnDeleteQuestion;
    private Button btnPostQuiz;
    private EditText etTitle;
    private ListView lvQuestions;
    private ProgressBar pbPostQuiz;
    private TextView tvQuestions;
    private TextView tvPostQuizErrorMessage;

    public RevisionQuizFormView(RevisionQuizzesActivity parent)
    {
        this.parent = parent;
        btnCreateQuestion = (Button)parent.findViewById(R.id.btnCreateQuestion);
        btnEditQuestion = (Button)parent.findViewById(R.id.btnEditQuestion);
        btnCloneQuestion = (Button)parent.findViewById(R.id.btnCloneQuestion);
        btnDeleteQuestion = (Button)parent.findViewById(R.id.btnDeleteQuestion);
        btnPostQuiz = (Button)parent.findViewById(R.id.btnPostQuiz);
        etTitle = (EditText)parent.findViewById(R.id.etTitle);
        lvQuestions = (ListView)parent.findViewById(R.id.lvQuestions);
        pbPostQuiz = (ProgressBar)parent.findViewById(R.id.pbPostQuiz);
        tvQuestions = (TextView)parent.findViewById(R.id.tvQuestions);
        tvPostQuizErrorMessage = (TextView)parent.findViewById(R.id.tvPostQuizErrorMessage);
        btnCreateQuestion.setOnClickListener(this);
        btnEditQuestion.setOnClickListener(this);
        btnCloneQuestion.setOnClickListener(this);
        btnDeleteQuestion.setOnClickListener(this);
        btnPostQuiz.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnCreateQuestion:
                tvQuestions.setError(null);
                parent.revisionQuestionFormView.createQuestion();
                parent.vfQuizzes.setDisplayedChild(2);
                break;
            case R.id.btnEditQuestion:
                editQuestion();
                break;
            case R.id.btnCloneQuestion:
                cloneQuestion();
                break;
            case R.id.btnDeleteQuestion:
                deleteQuestion();
                break;
            case R.id.btnPostQuiz:
                postQuiz();
                break;
        }
    }

    private Question getSelectedQuestion()
    {
        Question selectedQuestion = (Question)lvQuestions.getItemAtPosition(lvQuestions.getCheckedItemPosition());
        if(selectedQuestion == null)
        {
            tvQuestions.requestFocus();
            tvQuestions.setError(parent.getString(R.string.no_question_selected));
        }
        return selectedQuestion;
    }

    private void editQuestion()
    {
        tvQuestions.setError(null);
        parent.question = getSelectedQuestion();
        if(parent.question != null)
        {
            parent.revisionQuestionFormView.editQuestion();
            parent.vfQuizzes.setDisplayedChild(2);
        }
    }

    private void cloneQuestion()
    {
        tvQuestions.setError(null);
        Question question = getSelectedQuestion();
        if(question != null)
        {
            parent.revisionQuestionFormView.cloneQuestion(question);
            parent.vfQuizzes.setDisplayedChild(2);
        }
    }

    private void deleteQuestion()
    {
        tvQuestions.setError(null);
        Question selectedQuestion = getSelectedQuestion();
        if(selectedQuestion != null)
        {
            parent.quiz.removeQuestion(selectedQuestion.getQuestion());
            lvQuestions.setAdapter(new QuestionAdapter(parent, parent.quiz.getQuestions()));
        }
    }

    private void postQuiz()
    {
        etTitle.setError(null);
        tvQuestions.setError(null);
        if(!postQuizValidInputs())
        {
            return;
        }
        setPostQuizInProgress();
        parent.quiz.setTitle(etTitle.getText().toString());
        RevisionData.postQuiz(parent.quiz, postQuizOnSuccess, postQuizOnError);
    }

    protected void clearQuizForm()
    {
        tvQuestions.setError(null);
        etTitle.setError(null);
        etTitle.setText("");
        parent.quiz = new Quiz();
        lvQuestions.setAdapter(new QuestionAdapter(parent, parent.quiz.getQuestions()));
    }

    protected void updateQuestionList()
    {
        lvQuestions.setAdapter(new QuestionAdapter(parent, parent.quiz.getQuestions()));
    }

    private boolean postQuizValidInputs()
    {
        if(etTitle.getText().toString().isEmpty())
        {
            etTitle.requestFocus();
            etTitle.setError(parent.getString(R.string.title_empty));
            return false;
        }
        if(lvQuestions.getCount() == 0)
        {
            tvQuestions.requestFocus();
            tvQuestions.setError(parent.getString(R.string.no_questions_in_quiz));
            return false;
        }
        return true;
    }

    private void setPostQuizInProgress()
    {
        tvPostQuizErrorMessage.setText("");
        tvPostQuizErrorMessage.setVisibility(View.GONE);
        pbPostQuiz.setVisibility(View.VISIBLE);
        etTitle.setError(null);
        tvQuestions.setError(null);
        etTitle.setEnabled(false);
        btnCreateQuestion.setEnabled(false);
        btnEditQuestion.setEnabled(false);
        btnCloneQuestion.setEnabled(false);
        btnDeleteQuestion.setEnabled(false);
        btnPostQuiz.setEnabled(false);
    }

    private void setPostQuizComplete()
    {
        etTitle.setEnabled(true);
        btnCreateQuestion.setEnabled(true);
        btnEditQuestion.setEnabled(true);
        btnCloneQuestion.setEnabled(true);
        btnDeleteQuestion.setEnabled(true);
        btnPostQuiz.setEnabled(true);
        etTitle.setText("");
        pbPostQuiz.setVisibility(View.GONE);
    }

    private Runnable postQuizOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            parent.vfQuizzes.setDisplayedChild(0);
            setPostQuizComplete();
        }
    };

    private SingleArgRunnable<String> postQuizOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            tvPostQuizErrorMessage.setText(text);
            tvPostQuizErrorMessage.setVisibility(View.VISIBLE);
            setPostQuizComplete();
        }
    };


}
