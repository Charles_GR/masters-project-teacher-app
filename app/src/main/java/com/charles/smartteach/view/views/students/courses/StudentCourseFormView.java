package com.charles.smartteach.view.views.students.courses;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.smartteach.R;
import com.charles.smartteach.model.runnable.SingleArgRunnable;
import com.charles.smartteach.processing.database.CourseData;
import com.firebase.client.FirebaseError;

public class StudentCourseFormView implements OnClickListener
{

    private StudentCoursesActivity parent;
    private Button btnAddCourse;
    private Button btnSaveCourse;
    private EditText etCode;
    private EditText etTitle;
    private ProgressBar pbCourseForm;
    private TextView tvCourseFormErrorMessage;
    private ViewFlipper vfActionButton;

    public StudentCourseFormView(StudentCoursesActivity parent)
    {
        this.parent = parent;
        btnAddCourse = (Button)parent.findViewById(R.id.btnAddCourse);
        btnSaveCourse = (Button)parent.findViewById(R.id.btnSaveCourse);
        etCode = (EditText)parent.findViewById(R.id.etCode);
        etTitle = (EditText)parent.findViewById(R.id.etTitle);
        pbCourseForm = (ProgressBar)parent.findViewById(R.id.pbCourseForm);
        tvCourseFormErrorMessage = (TextView)parent.findViewById(R.id.tvCourseFormError);
        vfActionButton = (ViewFlipper)parent.findViewById(R.id.vfActionButton);
        btnAddCourse.setOnClickListener(this);
        btnSaveCourse.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnAddCourse:
                addCourse();
                break;
            case R.id.btnSaveCourse:
                saveCourse();
                break;
        }
    }

    protected void createAccount()
    {
        clearCourseForm();
        vfActionButton.setDisplayedChild(0);
    }

    protected void editCourseInfo()
    {
        vfActionButton.setDisplayedChild(1);
        etCode.setText(parent.course.getCode());
        etTitle.setText(parent.course.getTitle());
        parent.currentCourseCode = parent.course.getCode();
    }

    private void addCourse()
    {
        parent.hideKeyboard();
        clearCourseFormErrors();
        if(!courseFormValidInputs())
        {
            return;
        }
        btnAddCourse.setEnabled(false);
        pbCourseForm.setVisibility(View.VISIBLE);

        CourseData.addCourse(etCode.getText().toString(), etTitle.getText().toString(), addCourseOnSuccess, addCourseOnError);
    }

    private void saveCourse()
    {
        parent.hideKeyboard();
        clearCourseFormErrors();
        if(!courseFormValidInputs())
        {
            return;
        }
        btnSaveCourse.setEnabled(false);
        pbCourseForm.setVisibility(View.VISIBLE);
        CourseData.editCourse(etCode.getText().toString(), etTitle.getText().toString(), parent.currentCourseCode, saveCourseOnSuccess, saveCourseOnError);
    }

    protected void clearCourseForm()
    {
        etCode.setError(null);
        etTitle.setError(null);
        etCode.setText("");
        etTitle.setText("");
    }

    protected void clearCourseFormErrors()
    {
        etCode.setError(null);
        etTitle.setError(null);
        tvCourseFormErrorMessage.setText("");
    }

    private boolean courseFormValidInputs()
    {
        if(etCode.getText().toString().isEmpty())
        {
            etCode.requestFocus();
            etCode.setError(parent.getString(R.string.code_empty));
            return false;
        }
        if(etTitle.getText().toString().isEmpty())
        {
            etTitle.requestFocus();
            etTitle.setError(parent.getString(R.string.title_empty));
            return false;
        }
        return true;
    }

    private Runnable addCourseOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            pbCourseForm.setVisibility(View.GONE);
            parent.vfCourses.setDisplayedChild(0);
            etCode.setText("");
            etTitle.setText("");
            btnAddCourse.setEnabled(true);
        }
    };

    private SingleArgRunnable<String> addCourseOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String message)
        {
            pbCourseForm.setVisibility(View.GONE);
            tvCourseFormErrorMessage.setText(message);
            btnAddCourse.setEnabled(true);
        }
    };

    private Runnable saveCourseOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            pbCourseForm.setVisibility(View.GONE);
            parent.vfCourses.setDisplayedChild(0);
            btnSaveCourse.setEnabled(true);
        }
    };

    private SingleArgRunnable<FirebaseError> saveCourseOnError = new SingleArgRunnable<FirebaseError>()
    {
        @Override
        public void run(FirebaseError firebaseError)
        {
            if(firebaseError == null)
            {
                etCode.requestFocus();
                etCode.setError(parent.getString(R.string.code_in_use));
            }
            else
            {
                tvCourseFormErrorMessage.setText(firebaseError.getMessage());
            }
            pbCourseForm.setVisibility(View.GONE);
            btnSaveCourse.setEnabled(true);
        }
    };

}
