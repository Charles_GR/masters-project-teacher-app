package com.charles.smartteach.view.templates;

import android.app.Activity;
import android.content.Intent;
import android.view.inputmethod.InputMethodManager;
import com.charles.smartteach.model.TeachApplication;
import com.charles.smartteach.model.runnable.SingleArgRunnable;
import com.charles.smartteach.processing.database.FirebaseData;
import com.charles.smartteach.processing.database.LoginData;
import com.charles.smartteach.view.views.main.LobbyActivity;
import com.firebase.client.AuthData;

public abstract class AppActivity extends Activity
{

    protected void goToLobby()
    {
        startActivity(new Intent(this, LobbyActivity.class));
    }

    public void hideKeyboard()
    {
        if(getCurrentFocus() != null)
        {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public boolean checkCourseIsSelected()
    {
        if(TeachApplication.getInstance().getSelectedCourse() == null)
        {
            goToLobby();
            return false;
        }
        return true;
    }

    protected void checkLoginState()
    {
        AuthData authData = FirebaseData.baseRef.getAuth();
        if(authData == null)
        {
            finish();
        }
        else
        {
            LoginData.setEmail(authData.getProviderData().get("email").toString());
            LoginData.getUsernameFromEmail(LoginData.getEmail(), checkLoginOnSuccess, checkLoginOnError);
        }
    }

    private SingleArgRunnable<String> checkLoginOnSuccess = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            LoginData.setUsername(text);
        }
    };

    private SingleArgRunnable<String> checkLoginOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            FirebaseData.baseRef.unauth();
        }
    };

}
