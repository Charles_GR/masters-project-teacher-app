package com.charles.smartteach.view.views.revision.quizzes;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import com.charles.smartteach.R;
import com.charles.smartteach.model.adapters.revision.quizzes.FeedbackAdapter;
import com.charles.smartteach.model.adapters.revision.quizzes.AnswerAdapter;
import com.charles.smartteach.model.database.revision.quizzes.*;
import com.charles.smartteach.view.extras.PopupDialogs;

public class RevisionQuestionFormView implements OnClickListener, OnItemSelectedListener
{

    private RevisionQuizzesActivity parent;
    private Button btnCreateAnswer;
    private Button btnViewAnswer;
    private Button btnEditAnswer;
    private Button btnDeleteAnswer;
    private Button btnAddQuestion;
    private Button btnSaveQuestion;
    private Button btnCreateFeedback;
    private Button btnViewFeedback;
    private Button btnEditFeedback;
    private Button btnDeleteFeedback;
    private Button btnAddFeedback;
    private Button btnSaveFeedback;
    private EditText etQuestion;
    private EditText etCorrectAnswer;
    private EditText etTrueFeedback;
    private EditText etFalseFeedback;
    private EditText etFeedbackAnswer;
    private EditText etFeedbackValue;
    private ListView lvAnswers;
    private ListView lvFeedback;
    private Spinner spnAnswerType;
    private Switch swCorrectAnswer;
    private TextView tvAnswers;
    private TextView tvFeedback;
    private ViewFlipper vfQuestionActionButton;
    private ViewFlipper vfAnswers;
    private ViewFlipper vfFeedbackActionButton;

    private Feedback feedback;

    public RevisionQuestionFormView(RevisionQuizzesActivity parent)
    {
        this.parent = parent;
        btnCreateAnswer = (Button)parent.findViewById(R.id.btnCreateAnswer);
        btnViewAnswer = (Button)parent.findViewById(R.id.btnViewAnswer);
        btnEditAnswer = (Button)parent.findViewById(R.id.btnEditAnswer);
        btnDeleteAnswer = (Button)parent.findViewById(R.id.btnDeleteAnswer);
        btnAddQuestion = (Button)parent.findViewById(R.id.btnAddQuestion);
        btnSaveQuestion = (Button)parent.findViewById(R.id.btnSaveQuestion);
        btnCreateFeedback = (Button)parent.findViewById(R.id.btnCreateFeedback);
        btnViewFeedback = (Button)parent.findViewById(R.id.btnViewFeedback);
        btnEditFeedback = (Button)parent.findViewById(R.id.btnEditFeedback);
        btnDeleteFeedback = (Button)parent.findViewById(R.id.btnDeleteFeedback);
        btnAddFeedback = (Button)parent.findViewById(R.id.btnAddFeedback);
        btnSaveFeedback = (Button)parent.findViewById(R.id.btnSaveFeedback);
        etQuestion = (EditText)parent.findViewById(R.id.etQuestion);
        etCorrectAnswer = (EditText)parent.findViewById(R.id.etCorrectAnswer);
        etTrueFeedback = (EditText)parent.findViewById(R.id.etTrueFeedback);
        etFalseFeedback = (EditText)parent.findViewById(R.id.etFalseFeedback);
        etFeedbackAnswer = (EditText)parent.findViewById(R.id.etFeedbackAnswer);
        etFeedbackValue = (EditText)parent.findViewById(R.id.etFeedbackValue);
        lvAnswers = (ListView)parent.findViewById(R.id.lvAnswers);
        lvFeedback = (ListView)parent.findViewById(R.id.lvFeedback);
        spnAnswerType = (Spinner)parent.findViewById(R.id.spnAnswerType);
        swCorrectAnswer = (Switch)parent.findViewById(R.id.swCorrectAnswer);
        tvAnswers = (TextView)parent.findViewById(R.id.tvAnswers);
        tvFeedback = (TextView)parent.findViewById(R.id.tvFeedback);
        vfQuestionActionButton = (ViewFlipper)parent.findViewById(R.id.vfQuestionActionButton);
        vfAnswers = (ViewFlipper)parent.findViewById(R.id.vfAnswers);
        vfFeedbackActionButton = (ViewFlipper)parent.findViewById(R.id.vfFeedbackActionButton);
        btnCreateAnswer.setOnClickListener(this);
        btnViewAnswer.setOnClickListener(this);
        btnEditAnswer.setOnClickListener(this);
        btnDeleteAnswer.setOnClickListener(this);
        btnAddQuestion.setOnClickListener(this);
        btnSaveQuestion.setOnClickListener(this);
        btnCreateFeedback.setOnClickListener(this);
        btnViewFeedback.setOnClickListener(this);
        btnEditFeedback.setOnClickListener(this);
        btnDeleteFeedback.setOnClickListener(this);
        btnAddFeedback.setOnClickListener(this);
        btnSaveFeedback.setOnClickListener(this);
        spnAnswerType.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{SingleSelectQuestion.QUESTION_TYPE_TAG,
                MultiSelectQuestion.QUESTION_TYPE_TAG, TrueOrFalseQuestion.QUESTION_TYPE_TAG, ShortAnswerQuestion.QUESTION_TYPE_TAG}));
        spnAnswerType.setOnItemSelectedListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnCreateAnswer:
                createAnswer();
                break;
            case R.id.btnViewAnswer:
                viewAnswer();
                break;
            case R.id.btnEditAnswer:
                editAnswer();
                break;
            case R.id.btnDeleteAnswer:
                deleteAnswer();
                break;
            case R.id.btnAddQuestion:
                addQuestion();
                break;
            case R.id.btnSaveQuestion:
                saveQuestion();
                break;
            case R.id.btnCreateFeedback:
                createFeedback();
                break;
            case R.id.btnViewFeedback:
                viewFeedback();
                break;
            case R.id.btnEditFeedback:
                editFeedback();
                break;
            case R.id.btnDeleteFeedback:
                deleteFeedback();
                break;
            case R.id.btnAddFeedback:
                addFeedback();
                break;
            case R.id.btnSaveFeedback:
                saveFeedback();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        createQuestion(parent.getSelectedItem().toString());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    protected void createQuestion()
    {
        etQuestion.setError(null);
        etQuestion.setText("");
        silentSetAnswerTypeSelection(0);
        createQuestion(spnAnswerType.getSelectedItem().toString());
        vfQuestionActionButton.setDisplayedChild(0);
    }

    private void createQuestion(String type)
    {
        switch(type)
        {
            case SingleSelectQuestion.QUESTION_TYPE_TAG:
                parent.question = new SingleSelectQuestion();
                vfAnswers.setDisplayedChild(0);
                lvAnswers.setAdapter(new AnswerAdapter(parent));
                tvAnswers.setError(null);
                break;
            case MultiSelectQuestion.QUESTION_TYPE_TAG:
                parent.question = new MultiSelectQuestion();
                vfAnswers.setDisplayedChild(0);
                lvAnswers.setAdapter(new AnswerAdapter(parent));
                tvAnswers.setError(null);
                break;
            case TrueOrFalseQuestion.QUESTION_TYPE_TAG:
                parent.question = new TrueOrFalseQuestion();
                vfAnswers.setDisplayedChild(1);
                swCorrectAnswer.setChecked(false);
                etTrueFeedback.setError(null);
                etFalseFeedback.setError(null);
                etTrueFeedback.setText("");
                etFalseFeedback.setText("");
                break;
            case ShortAnswerQuestion.QUESTION_TYPE_TAG:
                parent.question = new ShortAnswerQuestion();
                vfAnswers.setDisplayedChild(2);
                lvFeedback.setAdapter(new FeedbackAdapter(parent));
                tvFeedback.setError(null);
                etCorrectAnswer.setError(null);
                etCorrectAnswer.setText("");
                break;
        }
    }

    protected void editQuestion()
    {
        etQuestion.setError(null);
        if(parent.question != null)
        {
            etQuestion.setText(parent.question.getQuestion());
            switch(parent.question.getType())
            {
                case SingleSelectQuestion.QUESTION_TYPE_TAG:
                    silentSetAnswerTypeSelection(0);
                    SingleSelectQuestion singleSelectQuestion = (SingleSelectQuestion)parent.question;
                    lvAnswers.setAdapter(new AnswerAdapter(parent, singleSelectQuestion.getAnswers()));
                    tvAnswers.setError(null);
                    break;
                case MultiSelectQuestion.QUESTION_TYPE_TAG:
                    silentSetAnswerTypeSelection(1);
                    MultiSelectQuestion multiSelectQuestion = (MultiSelectQuestion)parent.question;
                    lvAnswers.setAdapter(new AnswerAdapter(parent, multiSelectQuestion.getAnswers()));
                    tvAnswers.setError(null);
                    break;
                case TrueOrFalseQuestion.QUESTION_TYPE_TAG:
                    silentSetAnswerTypeSelection(2);
                    TrueOrFalseQuestion trueOrFalseQuestion = (TrueOrFalseQuestion)parent.question;
                    swCorrectAnswer.setChecked(trueOrFalseQuestion.getCorrectAnswer());
                    etTrueFeedback.setError(null);
                    etFalseFeedback.setError(null);
                    etTrueFeedback.setText(trueOrFalseQuestion.getFeedbackForTrue());
                    etFalseFeedback.setText(trueOrFalseQuestion.getFeedbackForFalse());
                    break;
                case ShortAnswerQuestion.QUESTION_TYPE_TAG:
                    silentSetAnswerTypeSelection(3);
                    ShortAnswerQuestion shortAnswerQuestion = (ShortAnswerQuestion)parent.question;
                    etCorrectAnswer.setError(null);
                    etCorrectAnswer.setText(shortAnswerQuestion.getCorrectAnswer());
                    lvFeedback.setAdapter(new FeedbackAdapter(parent, shortAnswerQuestion.createFeedbackList()));
                    break;
            }
            vfQuestionActionButton.setDisplayedChild(1);
        }
    }

    protected void cloneQuestion(Question selectedQuestion)
    {
        etQuestion.setText(selectedQuestion.getQuestion());
        switch(selectedQuestion.getType())
        {
            case SingleSelectQuestion.QUESTION_TYPE_TAG:
                parent.question = new SingleSelectQuestion();
                SingleSelectQuestion selectedSingleSelectQuestion = (SingleSelectQuestion)selectedQuestion;
                SingleSelectQuestion newSingleSelectQuestion = (SingleSelectQuestion)parent.question;
                for(Answer answer : selectedSingleSelectQuestion.getAnswers())
                {
                    SingleSelectAnswer singleSelectAnswer = (SingleSelectAnswer)answer;
                    newSingleSelectQuestion.addAnswer(singleSelectAnswer.getAnswer(), singleSelectAnswer.getCorrect(), singleSelectAnswer.getFeedback());
                }
                silentSetAnswerTypeSelection(0);
                vfAnswers.setDisplayedChild(0);
                lvAnswers.setAdapter(new AnswerAdapter(parent, newSingleSelectQuestion.getAnswers()));
                tvAnswers.setError(null);
                break;
            case MultiSelectQuestion.QUESTION_TYPE_TAG:
                parent.question = new MultiSelectQuestion();
                MultiSelectQuestion selectedMultiSelectQuestion = (MultiSelectQuestion)selectedQuestion;
                MultiSelectQuestion newMultiSelectQuestion = (MultiSelectQuestion)parent.question;
                for(Answer answer : selectedMultiSelectQuestion.getAnswers())
                {
                    MultiSelectAnswer multiSelectAnswer = (MultiSelectAnswer)answer;
                    newMultiSelectQuestion.addAnswer(multiSelectAnswer.getAnswer(), multiSelectAnswer.getCorrect(), multiSelectAnswer.getFeedbackForSelect(), multiSelectAnswer.getFeedbackForNotSelect());
                }
                silentSetAnswerTypeSelection(1);
                vfAnswers.setDisplayedChild(0);
                lvAnswers.setAdapter(new AnswerAdapter(parent, newMultiSelectQuestion.getAnswers()));
                tvAnswers.setError(null);
                break;
            case TrueOrFalseQuestion.QUESTION_TYPE_TAG:
                parent.question = new TrueOrFalseQuestion();
                TrueOrFalseQuestion selectedTrueOrFalseQuestion = (TrueOrFalseQuestion)selectedQuestion;
                silentSetAnswerTypeSelection(2);
                vfAnswers.setDisplayedChild(1);
                swCorrectAnswer.setChecked(selectedTrueOrFalseQuestion.getCorrectAnswer());
                break;
            case ShortAnswerQuestion.QUESTION_TYPE_TAG:
                parent.question = new ShortAnswerQuestion();
                ShortAnswerQuestion selectedShortAnswerQuestion = (ShortAnswerQuestion)selectedQuestion;
                ShortAnswerQuestion newShortAnswerQuestion = (ShortAnswerQuestion)parent.question;
                for(Feedback feedback : selectedShortAnswerQuestion.getFeedback().values())
                {
                    newShortAnswerQuestion.addFeedback(feedback.getAnswer(), feedback.getFeedback());
                }
                silentSetAnswerTypeSelection(3);
                vfAnswers.setDisplayedChild(2);
                lvFeedback.setAdapter(new FeedbackAdapter(parent, newShortAnswerQuestion.createFeedbackList()));
                etCorrectAnswer.setError(null);
                etCorrectAnswer.setText(selectedShortAnswerQuestion.getCorrectAnswer());
                break;
        }
    }

    private Answer getSelectedAnswer()
    {
        Answer selectedAnswer = (Answer)lvAnswers.getItemAtPosition(lvAnswers.getCheckedItemPosition());
        if(selectedAnswer == null)
        {
            tvAnswers.requestFocus();
            tvAnswers.setError(parent.getString(R.string.no_answer_selected));
        }
        return selectedAnswer;
    }

    private void createAnswer()
    {
        tvAnswers.setError(null);
        SelectQuestion selectQuestion = (SelectQuestion)parent.question;
        if(selectQuestion.getAnswers().size() == parent.getResources().getInteger(R.integer.max_answers))
        {
            tvAnswers.requestFocus();
            tvAnswers.setError(parent.getString(R.string.max_answers_reached));
        }
        else
        {
            parent.revisionAnswerFormView.createAnswer();
            parent.vfQuizzes.setDisplayedChild(3);
        }
    }

    private void viewAnswer()
    {
        tvAnswers.setError(null);
        Answer selectedAnswer = getSelectedAnswer();
        if(selectedAnswer != null)
        {
            PopupDialogs.showMessageDialog(parent, "View Answer", selectedAnswer.toString());
        }
    }

    private void editAnswer()
    {
        tvAnswers.setError(null);
        parent.answer = getSelectedAnswer();
        if(parent.answer != null)
        {
            parent.revisionAnswerFormView.editAnswer();
            parent.vfQuizzes.setDisplayedChild(3);
        }
    }

    @SuppressWarnings("unchecked")
    private void deleteAnswer()
    {
        SelectQuestion selectQuestion = (SelectQuestion)parent.question;
        Answer selectedAnswer = getSelectedAnswer();
        if(selectedAnswer != null)
        {
            selectQuestion.removeAnswer(selectedAnswer.getAnswer());
            lvAnswers.setAdapter(new AnswerAdapter(parent, selectQuestion.getAnswers()));
        }
    }

    private void addQuestion()
    {
        tvAnswers.setError(null);
        tvFeedback.setError(null);
        parent.question.setQuestion(etQuestion.getText().toString());
        if(!addOrEditQuestionValidInputs())
        {
            return;
        }
        switch(parent.question.getType())
        {
            case SingleSelectQuestion.QUESTION_TYPE_TAG:
            case MultiSelectQuestion.QUESTION_TYPE_TAG:
                SelectQuestion selectQuestion = (SelectQuestion)parent.question;
                String errorMessage = selectQuestion.checkAnswers(parent);
                if(errorMessage != null)
                {
                    tvAnswers.requestFocus();
                    tvAnswers.setError(errorMessage);
                    return;
                }
                break;
            case TrueOrFalseQuestion.QUESTION_TYPE_TAG:
                if(etTrueFeedback.getText().toString().isEmpty())
                {
                    etTrueFeedback.requestFocus();
                    etTrueFeedback.setError(parent.getString(R.string.feedback_needed));
                    return;
                }
                if(etFalseFeedback.getText().toString().isEmpty())
                {
                    etFalseFeedback.requestFocus();
                    etFalseFeedback.setError(parent.getString(R.string.feedback_needed));
                    return;
                }
                TrueOrFalseQuestion trueOrFalseQuestion = (TrueOrFalseQuestion)parent.question;
                trueOrFalseQuestion.setCorrectAnswer(swCorrectAnswer.isChecked());
                trueOrFalseQuestion.setFeedbackForTrue(etTrueFeedback.getText().toString());
                trueOrFalseQuestion.setFeedbackForFalse(etFalseFeedback.getText().toString());
                break;
            case ShortAnswerQuestion.QUESTION_TYPE_TAG:
                if(etCorrectAnswer.getText().toString().isEmpty())
                {
                    etCorrectAnswer.requestFocus();
                    etCorrectAnswer.setError(parent.getString(R.string.correct_answer_empty));
                    return;
                }
                ShortAnswerQuestion shortAnswerQuestion = (ShortAnswerQuestion)parent.question;
                shortAnswerQuestion.setCorrectAnswer(etCorrectAnswer.getText().toString());
                lvFeedback.setAdapter(new FeedbackAdapter(parent));
                break;
        }
        etQuestion.setText("");
        spnAnswerType.setSelection(0);
        lvAnswers.setAdapter(new AnswerAdapter(parent));
        parent.quiz.getQuestions().add(parent.question);
        parent.revisionQuizFormView.updateQuestionList();
        parent.vfQuizzes.setDisplayedChild(1);
    }

    private void saveQuestion()
    {
        tvAnswers.setError(null);
        tvFeedback.setError(null);
        parent.question.setQuestion(etQuestion.getText().toString());
        if(!addOrEditQuestionValidInputs())
        {
            return;
        }
        switch(parent.question.getType())
        {
            case SingleSelectQuestion.QUESTION_TYPE_TAG:
            case MultiSelectQuestion.QUESTION_TYPE_TAG:
                SelectQuestion selectQuestion = (SelectQuestion)parent.question;
                String errorMessage = selectQuestion.checkAnswers(parent);
                if(errorMessage != null)
                {
                    tvAnswers.requestFocus();
                    tvAnswers.setError(errorMessage);
                    return;
                }
                AnswerAdapter answerAdapter = (AnswerAdapter)lvAnswers.getAdapter();
                selectQuestion.setAnswers(answerAdapter.getAnswers());
                break;
            case TrueOrFalseQuestion.QUESTION_TYPE_TAG:
                if(etTrueFeedback.getText().toString().isEmpty())
                {
                    etTrueFeedback.requestFocus();
                    etTrueFeedback.setError(parent.getString(R.string.feedback_needed));
                    return;
                }
                if(etFalseFeedback.getText().toString().isEmpty())
                {
                    etFalseFeedback.requestFocus();
                    etFalseFeedback.setError(parent.getString(R.string.feedback_needed));
                    return;
                }
                TrueOrFalseQuestion trueOrFalseQuestion = (TrueOrFalseQuestion)parent.question;
                trueOrFalseQuestion.setCorrectAnswer(swCorrectAnswer.isChecked());
                trueOrFalseQuestion.setFeedbackForTrue(etTrueFeedback.getText().toString());
                trueOrFalseQuestion.setFeedbackForFalse(etFalseFeedback.getText().toString());
                break;
            case ShortAnswerQuestion.QUESTION_TYPE_TAG:
                if(etCorrectAnswer.getText().toString().isEmpty())
                {
                    etCorrectAnswer.requestFocus();
                    etCorrectAnswer.setError(parent.getString(R.string.correct_answer_empty));
                    return;
                }
                ShortAnswerQuestion shortAnswerQuestion = (ShortAnswerQuestion)parent.question;
                shortAnswerQuestion.setCorrectAnswer(etCorrectAnswer.getText().toString());
                break;
        }
        etQuestion.setText("");
        lvAnswers.setAdapter(new AnswerAdapter(parent));
        parent.revisionQuizFormView.updateQuestionList();
        parent.vfQuizzes.setDisplayedChild(1);
    }

    protected void updateAnswerList()
    {
        SelectQuestion selectQuestion = (SelectQuestion)parent.question;
        lvAnswers.setAdapter(new AnswerAdapter(parent, selectQuestion.getAnswers()));
    }

    private Feedback getSelectedFeedback()
    {
        Feedback selectedFeedback = (Feedback)lvFeedback.getItemAtPosition(lvFeedback.getCheckedItemPosition());
        if(selectedFeedback == null)
        {
            tvFeedback.requestFocus();
            tvFeedback.setError(parent.getString(R.string.no_feedback_selected));
        }
        return selectedFeedback;
    }

    private void createFeedback()
    {
        tvFeedback.setError(null);
        etFeedbackAnswer.setError(null);
        etFeedbackValue.setError(null);
        etFeedbackAnswer.setText("");
        etFeedbackValue.setText("");
        vfFeedbackActionButton.setDisplayedChild(0);
        parent.vfQuizzes.setDisplayedChild(4);
    }

    private void viewFeedback()
    {
        tvFeedback.setError(null);
        Feedback selectedFeedback = getSelectedFeedback();
        if(selectedFeedback != null)
        {
            PopupDialogs.showMessageDialog(parent, "View Feedback", selectedFeedback.toString());
        }
    }

    protected void editFeedback()
    {
        tvFeedback.setError(null);
        etFeedbackAnswer.setError(null);
        etFeedbackValue.setError(null);
        feedback = getSelectedFeedback();
        if(feedback != null)
        {
            etFeedbackAnswer.setText(feedback.getAnswer());
            etFeedbackValue.setText(feedback.getFeedback());
            vfFeedbackActionButton.setDisplayedChild(1);
            parent.vfQuizzes.setDisplayedChild(4);
        }
    }

    private void deleteFeedback()
    {
        tvFeedback.setError(null);
        ShortAnswerQuestion shortAnswerQuestion = (ShortAnswerQuestion)parent.question;
        Feedback selectedFeedback = getSelectedFeedback();
        if(selectedFeedback != null)
        {
            shortAnswerQuestion.removeFeedback(selectedFeedback.getAnswer());
            lvFeedback.setAdapter(new FeedbackAdapter(parent, shortAnswerQuestion.createFeedbackList()));
        }
    }

    private void addFeedback()
    {
        etFeedbackAnswer.setError(null);
        etFeedbackValue.setError(null);
        parent.hideKeyboard();
        if(!addOrEditFeedbackValidInputs())
        {
            return;
        }
        ShortAnswerQuestion shortAnswerQuestion = (ShortAnswerQuestion)parent.question;
        if(!shortAnswerQuestion.addFeedback(etFeedbackAnswer.getText().toString(), etFeedbackValue.getText().toString()))
        {
            etFeedbackAnswer.requestFocus();
            etFeedbackAnswer.setError(parent.getString(R.string.feedback_already_added));
            return;
        }
        lvFeedback.setAdapter(new FeedbackAdapter(parent, shortAnswerQuestion.createFeedbackList()));
        etFeedbackAnswer.setText("");
        etFeedbackValue.setText("");
        parent.vfQuizzes.setDisplayedChild(2);
    }

    private void saveFeedback()
    {
        etFeedbackAnswer.setError(null);
        etFeedbackValue.setError(null);
        parent.hideKeyboard();
        if(!addOrEditFeedbackValidInputs())
        {
            return;
        }
        ShortAnswerQuestion shortAnswerQuestion = (ShortAnswerQuestion)parent.question;
        shortAnswerQuestion.editFeedback(feedback.getAnswer(), etFeedbackAnswer.getText().toString(), etFeedbackValue.getText().toString());
        lvFeedback.setAdapter(new FeedbackAdapter(parent, shortAnswerQuestion.createFeedbackList()));
        parent.vfQuizzes.setDisplayedChild(2);
    }

    private void silentSetAnswerTypeSelection(int position)
    {
        spnAnswerType.setOnItemSelectedListener(null);
        spnAnswerType.setSelection(position);
        spnAnswerType.setOnItemSelectedListener(this);
    }

    private boolean addOrEditQuestionValidInputs()
    {
        if(etQuestion.getText().toString().isEmpty())
        {
            etQuestion.requestFocus();
            etQuestion.setError(parent.getString(R.string.question_empty));
            return false;
        }
        return true;
    }

    private boolean addOrEditFeedbackValidInputs()
    {
        if(etFeedbackAnswer.getText().toString().isEmpty())
        {
            etFeedbackAnswer.requestFocus();
            etFeedbackAnswer.setError(parent.getString(R.string.answer_empty));
            return false;
        }
        if(etFeedbackValue.getText().toString().isEmpty())
        {
            etFeedbackValue.requestFocus();
            etFeedbackValue.setError(parent.getString(R.string.feedback_empty));
            return false;
        }
        return true;
    }

}
