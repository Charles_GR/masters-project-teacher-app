package com.charles.smartteach.view.views.students.courses;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import com.charles.smartteach.R;
import com.charles.smartteach.model.adapters.students.StudentAdapter;
import com.charles.smartteach.model.database.students.User;
import com.charles.smartteach.model.runnable.ListRunnable;
import com.charles.smartteach.model.runnable.SingleArgRunnable;
import com.charles.smartteach.processing.database.CourseData;
import com.charles.smartteach.processing.database.FirebaseData;
import java.util.ArrayList;
import java.util.List;

public class StudentCourseStudentListView implements OnClickListener, OnItemSelectedListener, TextWatcher
{

    private StudentCoursesActivity parent;
    private Button btnAddCourseStudent;
    private Button btnRemoveCourseStudent;
    private EditText etStudentSearchQuery;
    private ListView lvCourseStudents;
    private Spinner spnStudentSortField;
    private Spinner spnStudentSortOrdering;
    private Spinner spnStudentSearchField;
    private Spinner spnCourseStudent;
    private TextView tvCourseStudents;
    private TextView tvNoCourseStudentsYet;
    private TextView tvCourseStudentsErrorMessage;

    public StudentCourseStudentListView(StudentCoursesActivity parent)
    {
        this.parent = parent;
        btnAddCourseStudent = (Button)parent.findViewById(R.id.btnAddCourseStudent);
        btnRemoveCourseStudent = (Button)parent.findViewById(R.id.btnRemoveCourseStudent);
        etStudentSearchQuery = (EditText)parent.findViewById(R.id.etStudentSearchQuery);
        lvCourseStudents = (ListView)parent.findViewById(R.id.lvCourseStudents);
        spnStudentSortField = (Spinner)parent.findViewById(R.id.spnStudentSortField);
        spnStudentSortOrdering = (Spinner)parent.findViewById(R.id.spnStudentSortOrdering);
        spnStudentSearchField = (Spinner)parent.findViewById(R.id.spnStudentSearchField);
        spnCourseStudent = (Spinner)parent.findViewById(R.id.spnCourseStudent);
        tvCourseStudents = (TextView)parent.findViewById(R.id.tvCourseStudents);
        tvNoCourseStudentsYet = (TextView)parent.findViewById(R.id.tvNoCourseStudentsYet);
        tvCourseStudentsErrorMessage = (TextView)parent.findViewById(R.id.tvCourseStudentsErrorMessage);
        btnAddCourseStudent.setOnClickListener(this);
        btnRemoveCourseStudent.setOnClickListener(this);
        etStudentSearchQuery.addTextChangedListener(this);
        spnStudentSortField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Full Name", "Username", "Email"}));
        spnStudentSortField.setOnItemSelectedListener(this);
        spnStudentSortOrdering.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Ascending", "Descending"}));
        spnStudentSortOrdering.setOnItemSelectedListener(this);
        spnStudentSearchField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Full Name", "Username", "Email"}));
        spnStudentSearchField.setOnItemSelectedListener(this);
        spnCourseStudent.setOnItemSelectedListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnAddCourseStudent:
                addCourseStudent();
                break;
            case R.id.btnRemoveCourseStudent:
                removeCourseStudent();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        switch(parent.getId())
        {
            case R.id.spnStudentSortField:
            case R.id.spnStudentSortOrdering:
            case R.id.spnStudentSearchField:
                searchAndSortCourseStudents();
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        searchAndSortCourseStudents();
    }

    @Override
    public void afterTextChanged(Editable s)
    {

    }

    protected void getCourseStudents()
    {
        parent.currentCourseCode = parent.course.getCode();
        CourseData.getCourseStudents(parent.currentCourseCode, getCourseStudentsOnSuccess, getCourseStudentsOnError);
        CourseData.getNonCourseStudents(parent.currentCourseCode, getNonCourseStudentsOnSuccess, getNonCourseStudentsOnError);
    }

    private void searchAndSortCourseStudents()
    {
        lvCourseStudents.clearChoices();
        StudentAdapter studentAdapter = (StudentAdapter)lvCourseStudents.getAdapter();
        if(studentAdapter != null)
        {
            studentAdapter.searchItems(spnStudentSearchField.getSelectedItem().toString(), etStudentSearchQuery.getText().toString());
            studentAdapter.sortItems(spnStudentSortField.getSelectedItem().toString(), spnStudentSortOrdering.getSelectedItem().toString());
        }
    }

    private void addCourseStudent()
    {
        tvCourseStudents.setError(null);
        User selectedStudent = (User)spnCourseStudent.getSelectedItem();
        if(selectedStudent == null)
        {
            tvCourseStudents.requestFocus();
            tvCourseStudents.setError(parent.getString(R.string.no_more_students));
            return;
        }
        FirebaseData.getCourseStudentsRef(parent.currentCourseCode).push().setValue(selectedStudent.getUsername());
    }

    private void removeCourseStudent()
    {
        tvCourseStudents.setError(null);
        User selectedStudent = (User)lvCourseStudents.getItemAtPosition(lvCourseStudents.getCheckedItemPosition());
        if(selectedStudent == null)
        {
            tvCourseStudents.requestFocus();
            tvCourseStudents.setError(parent.getString(R.string.no_student_selected));
            return;
        }
        CourseData.removeStudentFromCourse(parent.currentCourseCode, selectedStudent.getUsername(), deleteCourseStudentOnError);
    }

    private ListRunnable<User> getCourseStudentsOnSuccess = new ListRunnable<User>()
    {
        @Override
        public void run(List<User> students)
        {
            lvCourseStudents.setAdapter(new StudentAdapter(parent, students));
            tvNoCourseStudentsYet.setVisibility(students.isEmpty() ? View.VISIBLE : View.GONE);
            searchAndSortCourseStudents();
        }
    };

    private SingleArgRunnable<String> getCourseStudentsOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            lvCourseStudents.setAdapter(new StudentAdapter(parent));
            tvNoCourseStudentsYet.setVisibility(View.GONE);
            tvCourseStudentsErrorMessage.setVisibility(View.VISIBLE);
            tvCourseStudentsErrorMessage.setText(text);
        }
    };

    private ListRunnable<User> getNonCourseStudentsOnSuccess = new ListRunnable<User>()
    {
        @Override
        public void run(List<User> students)
        {
            spnCourseStudent.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, students));
        }
    };

    private SingleArgRunnable<String> getNonCourseStudentsOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            spnCourseStudent.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new ArrayList<User>()));
            tvCourseStudentsErrorMessage.setVisibility(View.VISIBLE);
            tvCourseStudentsErrorMessage.setText(text);
        }
    };

    private SingleArgRunnable<String> deleteCourseStudentOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            tvCourseStudentsErrorMessage.setVisibility(View.VISIBLE);
            tvCourseStudentsErrorMessage.setText(text);
        }
    };

}
