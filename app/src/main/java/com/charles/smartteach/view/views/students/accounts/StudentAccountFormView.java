package com.charles.smartteach.view.views.students.accounts;

import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.smartteach.R;
import com.charles.smartteach.model.runnable.SingleArgRunnable;
import com.charles.smartteach.processing.database.StudentData;
import com.firebase.client.FirebaseError;

public class StudentAccountFormView implements OnClickListener
{

    private StudentAccountsActivity parent;
    private Button btnAddAccount;
    private Button btnSaveAccount;
    private EditText etFirstName;
    private EditText etLastName;
    private EditText etUsername;
    private EditText etEmail;
    private LinearLayout llEmail;
    private ProgressBar pbAccountForm;
    private ViewFlipper vfActionButton;
    private TextView tvAccountFormError;

    public StudentAccountFormView(StudentAccountsActivity parent)
    {
        this.parent = parent;
        btnAddAccount = (Button)parent.findViewById(R.id.btnAddAccount);
        btnSaveAccount = (Button)parent.findViewById(R.id.btnSaveAccount);
        etFirstName = (EditText)parent.findViewById(R.id.etFirstName);
        etLastName = (EditText)parent.findViewById(R.id.etLastName);
        etUsername = (EditText)parent.findViewById(R.id.etUsername);
        etEmail = (EditText)parent.findViewById(R.id.etEmail);
        llEmail = (LinearLayout)parent.findViewById(R.id.llEmail);
        pbAccountForm = (ProgressBar)parent.findViewById(R.id.pbAccountForm);
        tvAccountFormError = (TextView)parent.findViewById(R.id.tvAccountFormError);
        vfActionButton = (ViewFlipper)parent.findViewById(R.id.vfActionButton);
        btnAddAccount.setOnClickListener(this);
        btnSaveAccount.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnAddAccount:
                addAccount();
                break;
            case R.id.btnSaveAccount:
                saveAccount();
                break;
        }
    }

    protected void createAccount()
    {
        accountFormClearErrors();
        etFirstName.setText("");
        etLastName.setText("");
        etUsername.setText("");
        etEmail.setText("");
        llEmail.setVisibility(View.VISIBLE);
        vfActionButton.setDisplayedChild(0);
    }

    protected void editAccount()
    {
        accountFormClearErrors();
        etFirstName.setText(parent.student.getFirstName());
        etLastName.setText(parent.student.getLastName());
        etUsername.setText(parent.student.getUsername());
        llEmail.setVisibility(View.GONE);
        parent.currentUsername = parent.student.getUsername();
        vfActionButton.setDisplayedChild(1);
    }

    private void addAccount()
    {
        parent.hideKeyboard();
        accountFormClearErrors();
        if(!accountFormValidInputs())
        {
            return;
        }
        btnAddAccount.setEnabled(false);
        pbAccountForm.setVisibility(View.VISIBLE);
        StudentData.addStudent(etFirstName.getText().toString(), etLastName.getText().toString(), etUsername.getText().toString(), etEmail.getText().toString(), addStudentOnSuccess, addStudentOnError);
    }

    private void saveAccount()
    {
        parent.hideKeyboard();
        accountFormClearErrors();
        if(!accountFormValidInputs())
        {
            return;
        }
        btnSaveAccount.setEnabled(false);
        pbAccountForm.setVisibility(View.VISIBLE);
        StudentData.editStudent(etFirstName.getText().toString(), etLastName.getText().toString(), parent.currentUsername, etUsername.getText().toString(), saveStudentOnSuccess, saveStudentOnError);
    }

    private void accountFormClearErrors()
    {
        etFirstName.setError(null);
        etLastName.setError(null);
        etUsername.setError(null);
        etEmail.setError(null);
        tvAccountFormError.setText("");
    }

    private boolean accountFormValidInputs()
    {
        if(etFirstName.getText().toString().isEmpty())
        {
            etFirstName.requestFocus();
            etFirstName.setError(parent.getString(R.string.first_name_empty));
            return false;
        }
        if(etLastName.getText().toString().isEmpty())
        {
            etLastName.requestFocus();
            etLastName.setError(parent.getString(R.string.last_name_empty));
            return false;
        }
        if(etUsername.getText().toString().isEmpty())
        {
            etUsername.requestFocus();
            etUsername.setError(parent.getString(R.string.username_empty));
            return false;
        }
        if(etUsername.getText().toString().contains("@"))
        {
            etUsername.requestFocus();
            etUsername.setError(parent.getString(R.string.username_invalid));
            return false;
        }
        if(llEmail.getVisibility() == View.VISIBLE && etEmail.getText().toString().isEmpty())
        {
            etEmail.requestFocus();
            etEmail.setError(parent.getString(R.string.email_empty));
            return false;
        }
        if(llEmail.getVisibility() == View.VISIBLE && !Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches())
        {
            etEmail.requestFocus();
            etEmail.setError(parent.getString(R.string.email_invalid));
            return false;
        }
        return true;
    }

    private Runnable addStudentOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            pbAccountForm.setVisibility(View.GONE);
            parent.vfAccounts.setDisplayedChild(0);
            etFirstName.setText("");
            etLastName.setText("");
            etUsername.setText("");
            etEmail.setText("");
            btnAddAccount.setEnabled(true);
        }
    };

    private SingleArgRunnable<String> addStudentOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String message)
        {
            pbAccountForm.setVisibility(View.GONE);
            tvAccountFormError.setText(message);
            btnAddAccount.setEnabled(true);
        }
    };

    private Runnable saveStudentOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            pbAccountForm.setVisibility(View.GONE);
            parent.vfAccounts.setDisplayedChild(0);
            btnSaveAccount.setEnabled(true);
        }
    };

    private SingleArgRunnable<FirebaseError> saveStudentOnError = new SingleArgRunnable<FirebaseError>()
    {
        @Override
        public void run(FirebaseError firebaseError)
        {
            if(firebaseError == null)
            {
                etUsername.requestFocus();
                etUsername.setError(parent.getString(R.string.username_already_taken));
            }
            else
            {
                tvAccountFormError.setText(firebaseError.getMessage());
            }
            pbAccountForm.setVisibility(View.GONE);
            btnSaveAccount.setEnabled(true);
        }
    };

}
