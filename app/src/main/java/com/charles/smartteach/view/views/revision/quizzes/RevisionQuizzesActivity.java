package com.charles.smartteach.view.views.revision.quizzes;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.smartteach.R;
import com.charles.smartteach.model.database.revision.quizzes.*;
import com.charles.smartteach.view.templates.AppActivity;
import com.charles.smartteach.view.views.revision.RevisionActivity;

public class RevisionQuizzesActivity extends AppActivity implements OnClickListener
{

    private static RevisionQuizzesActivity instance;

    protected RevisionQuizzesListView revisionQuizzesListView;
    protected RevisionQuizFormView revisionQuizFormView;
    protected RevisionQuestionFormView revisionQuestionFormView;
    protected RevisionAnswerFormView revisionAnswerFormView;
    protected RevisionQuizResultsView revisionQuizResultsView;
    protected ViewFlipper vfQuizzes;

    protected Quiz quiz;
    protected Question question;
    protected Answer answer;

    public static RevisionQuizzesActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(!checkCourseIsSelected())
        {
            return;
        }
        instance = this;
        setContentView(R.layout.activity_revision_quizzes);
        revisionQuizzesListView = new RevisionQuizzesListView(this);
        revisionQuizFormView = new RevisionQuizFormView(this);
        revisionQuestionFormView = new RevisionQuestionFormView(this);
        revisionAnswerFormView = new RevisionAnswerFormView(this);
        revisionQuizResultsView = new RevisionQuizResultsView(this);
        vfQuizzes = (ViewFlipper)findViewById(R.id.vfQuizzes);
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        outState.putInt("currentQuizView", vfQuizzes.getDisplayedChild());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState)
    {
        vfQuizzes.setDisplayedChild(savedInstanceState.getInt("currentQuizView"));
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onClick(View v)
    {
        RevisionActivity.getInstance().onClick(v);
    }

    public void onUpButtonPressed()
    {
        switch(vfQuizzes.getDisplayedChild())
        {
            case 0:
                goToLobby();
                break;
            case 1:
                vfQuizzes.setDisplayedChild(0);
                break;
            case 2:
                vfQuizzes.setDisplayedChild(1);
                break;
            case 3:
                vfQuizzes.setDisplayedChild(2);
                break;
            case 4:
                vfQuizzes.setDisplayedChild(2);
                break;
            case 5:
                vfQuizzes.setDisplayedChild(0);
                break;
            case 6:
                vfQuizzes.setDisplayedChild(5);
                break;
            case 7:
                vfQuizzes.setDisplayedChild(6);
                break;
        }
    }

}