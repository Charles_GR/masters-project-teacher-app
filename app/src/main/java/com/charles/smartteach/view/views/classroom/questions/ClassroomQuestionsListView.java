package com.charles.smartteach.view.views.classroom.questions;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import com.charles.smartteach.R;
import com.charles.smartteach.model.adapters.classroom.questions.AnswerAdapter;
import com.charles.smartteach.model.adapters.classroom.questions.QuestionAdapter;
import com.charles.smartteach.model.database.classroom.questions.Question;
import com.charles.smartteach.model.runnable.ListRunnable;
import com.charles.smartteach.model.runnable.SingleArgRunnable;
import com.charles.smartteach.processing.database.ClassroomData;
import com.charles.smartteach.processing.database.FirebaseData;
import java.util.List;

public class ClassroomQuestionsListView implements OnClickListener, OnItemSelectedListener, TextWatcher
{

    private ClassroomQuestionsActivity parent;
    private Button btnCreateQuestion;
    private Button btnViewQuestion;
    private Button btnCloneQuestion;
    private Button btnDeleteSelectedQuestion;
    private EditText etSearchQuery;
    private ListView lvQuestions;
    private ProgressBar pbLoadQuestions;
    private Spinner spnSortField;
    private Spinner spnSortOrdering;
    private Spinner spnSearchField;
    private TextView tvQuestions;
    private TextView tvNoQuestionsYet;
    private TextView tvQuestionListErrorMessage;

    public ClassroomQuestionsListView(ClassroomQuestionsActivity parent)
    {
        this.parent = parent;
        btnCreateQuestion = (Button)parent.findViewById(R.id.btnCreateQuestion);
        btnViewQuestion = (Button)parent.findViewById(R.id.btnViewQuestion);
        btnCloneQuestion = (Button)parent.findViewById(R.id.btnCloneQuestion);
        btnDeleteSelectedQuestion = (Button)parent.findViewById(R.id.btnDeleteSelectedQuestion);
        etSearchQuery = (EditText)parent.findViewById(R.id.etSearchQuery);
        lvQuestions = (ListView)parent.findViewById(R.id.lvQuestions);
        pbLoadQuestions = (ProgressBar)parent.findViewById(R.id.pbLoadQuestions);
        spnSortField = (Spinner)parent.findViewById(R.id.spnSortField);
        spnSortOrdering = (Spinner)parent.findViewById(R.id.spnSortOrdering);
        spnSearchField = (Spinner)parent.findViewById(R.id.spnSearchField);
        tvQuestions = (TextView)parent.findViewById(R.id.tvQuestions);
        tvNoQuestionsYet = (TextView)parent.findViewById(R.id.tvNoQuestionsYet);
        tvQuestionListErrorMessage = (TextView)parent.findViewById(R.id.tvQuestionListErrorMessage);
        btnCreateQuestion.setOnClickListener(this);
        btnViewQuestion.setOnClickListener(this);
        btnCloneQuestion.setOnClickListener(this);
        btnDeleteSelectedQuestion.setOnClickListener(this);
        etSearchQuery.addTextChangedListener(this);
        spnSortField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Question", "Date Created", "# Responses"}));
        spnSortField.setOnItemSelectedListener(this);
        spnSortOrdering.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Ascending", "Descending"}));
        spnSortOrdering.setOnItemSelectedListener(this);
        spnSearchField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Question"}));
        spnSearchField.setOnItemSelectedListener(this);
        ClassroomData.getQuestions(getQuestionsOnSuccess, getQuestionsOnError);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnCreateQuestion:
                parent.classroomQuestionFormView.createQuestion();
                parent.vfQuestions.setDisplayedChild(1);
                break;
            case R.id.btnViewQuestion:
                viewQuestion();
                break;
            case R.id.btnCloneQuestion:
                cloneQuestion();
                break;
            case R.id.btnDeleteSelectedQuestion:
                deleteSelectedQuestion();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        searchAndSortQuestions();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        searchAndSortQuestions();
    }

    @Override
    public void afterTextChanged(Editable s)
    {

    }

    private void searchAndSortQuestions()
    {
        lvQuestions.clearChoices();
        QuestionAdapter questionAdapter = (QuestionAdapter)lvQuestions.getAdapter();
        if(questionAdapter != null)
        {
            questionAdapter.searchItems(spnSearchField.getSelectedItem().toString(), etSearchQuery.getText().toString());
            questionAdapter.sortItems(spnSortField.getSelectedItem().toString(), spnSortOrdering.getSelectedItem().toString());
        }
    }

    private Question getSelectedQuestion()
    {
        Question selectedQuestion = (Question)lvQuestions.getItemAtPosition(lvQuestions.getCheckedItemPosition());
        if(selectedQuestion == null)
        {
            tvQuestions.requestFocus();
            tvQuestions.setError(parent.getString(R.string.no_question_selected));
        }
        return selectedQuestion;
    }

    private void viewQuestion()
    {
        tvQuestions.setError(null);
        parent.question = getSelectedQuestion();
        if(parent.question == null)
        {
            return;
        }
        parent.classroomQuestionResultsView.displayResults(parent.question);
        parent.vfQuestions.setDisplayedChild(3);
    }

    private void cloneQuestion()
    {
        tvQuestions.setError(null);
        parent.question = getSelectedQuestion();
        if(parent.question == null)
        {
            return;
        }
        parent.classroomQuestionFormView.cloneQuestion();
        parent.vfQuestions.setDisplayedChild(1);
    }


    private void deleteSelectedQuestion()
    {
        tvQuestions.setError(null);
        Question selectedQuestion = getSelectedQuestion();
        if(selectedQuestion != null)
        {
            FirebaseData.getClassroomQuestionsRef().child(selectedQuestion.getKey()).removeValue();
        }
    }

    private ListRunnable<Question> getQuestionsOnSuccess = new ListRunnable<Question>()
    {
        @Override
        public void run(List<Question> questions)
        {
            lvQuestions.setAdapter(new QuestionAdapter(parent, questions));
            tvNoQuestionsYet.setVisibility(questions.isEmpty() ? View.VISIBLE : View.GONE);
            pbLoadQuestions.setVisibility(View.GONE);
            parent.classroomQuestionResultsView.displayResults(questions);
            searchAndSortQuestions();
        }
    };

    private SingleArgRunnable<String> getQuestionsOnError = new  SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            lvQuestions.setAdapter(new AnswerAdapter(parent));
            tvNoQuestionsYet.setVisibility(View.GONE);
            pbLoadQuestions.setVisibility(View.GONE);
            tvQuestionListErrorMessage.setText(text);
            tvQuestionListErrorMessage.setVisibility(View.VISIBLE);
        }
    };

}
