package com.charles.smartteach.view.views.revision.quizzes;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
import com.charles.smartteach.R;
import com.charles.smartteach.model.database.revision.quizzes.Quiz;
import com.charles.smartteach.view.extras.DataCharts;
import com.charles.smartteach.view.views.revision.RevisionActivity;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import java.util.HashMap;
import java.util.List;

public class RevisionQuizResultsView implements OnClickListener, OnItemSelectedListener, OnCheckedChangeListener
{

    private RevisionQuizzesActivity parent;
    private BarChart bcResults;
    private Button btnViewQuestionResults;
    private Button btnViewChart;
    private Button btnPreviousQuestion;
    private Button btnNextQuestion;
    private PieChart pcResults;
    private Spinner spnStudentA;
    private Spinner spnStudentB;
    private Switch swChartType;
    private TextView tvQuizTitle;
    private TextView tvBestScore;
    private TextView tvWorstScore;
    private TextView tvAverageScore;
    private TextView tvStudentScore;
    private TextView tvQuestion;
    private TextView tvResults;
    private TextView tvStudentAnswers;
    private ViewFlipper vfDataCharts;

    private int questionIndex;

    public RevisionQuizResultsView(RevisionQuizzesActivity parent)
    {
        this.parent = parent;
        bcResults = (BarChart)parent.findViewById(R.id.bcResults);
        btnViewQuestionResults = (Button)parent.findViewById(R.id.btnViewQuestionResults);
        btnViewChart = (Button)parent.findViewById(R.id.btnViewChart);
        btnPreviousQuestion = (Button)parent.findViewById(R.id.btnPreviousQuestion);
        btnNextQuestion = (Button)parent.findViewById(R.id.btnNextQuestion);
        pcResults = (PieChart)parent.findViewById(R.id.pcResults);
        spnStudentA = (Spinner)parent.findViewById(R.id.spnStudentA);
        spnStudentB = (Spinner)parent.findViewById(R.id.spnStudentB);
        swChartType = (Switch)parent.findViewById(R.id.swChartType);
        tvQuizTitle = (TextView)parent.findViewById(R.id.tvQuizTitle);
        tvBestScore = (TextView)parent.findViewById(R.id.tvBestScoreValue);
        tvWorstScore = (TextView)parent.findViewById(R.id.tvWorstScoreValue);
        tvAverageScore = (TextView)parent.findViewById(R.id.tvAverageScoreValue);
        tvStudentScore = (TextView)parent.findViewById(R.id.tvStudentScoreValue);
        tvQuestion = (TextView)parent.findViewById(R.id.tvQuestionB);
        tvResults = (TextView)parent.findViewById(R.id.tvResults);
        tvStudentAnswers = (TextView)parent.findViewById(R.id.tvStudentAnswersValue);
        vfDataCharts = (ViewFlipper)parent.findViewById(R.id.vfDataCharts);
        btnViewQuestionResults.setOnClickListener(this);
        btnViewChart.setOnClickListener(this);
        btnPreviousQuestion.setOnClickListener(this);
        btnNextQuestion.setOnClickListener(this);
        spnStudentA.setOnItemSelectedListener(this);
        spnStudentB.setOnItemSelectedListener(this);
        swChartType.setOnCheckedChangeListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnViewQuestionResults:
                questionIndex = 0;
                viewQuestionResults();
                parent.vfQuizzes.setDisplayedChild(6);
                break;
            case R.id.btnViewChart:
                viewChart();
                break;
            case R.id.btnPreviousQuestion:
                questionIndex--;
                viewQuestionResults();
                break;
            case R.id.btnNextQuestion:
                questionIndex++;
                viewQuestionResults();
                break;
            default:
                RevisionActivity.getInstance().onClick(v);
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        switch(parent.getId())
        {
            case R.id.spnStudentA:
                tvStudentScore.setText(this.parent.quiz.getStudentScore(spnStudentA.getSelectedItem().toString()));
                break;
            case R.id.spnStudentB:
                tvStudentAnswers.setText(this.parent.quiz.getResponses().get(spnStudentB.getSelectedItem().toString()).answersToString(questionIndex));
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
    {
        if(buttonView.getId() == R.id.swChartType)
        {
            vfDataCharts.setDisplayedChild(swChartType.isChecked() ? 1 : 0);
        }
    }

    private void viewQuestionResults()
    {
        parent.question = parent.quiz.getQuestions().get(questionIndex);
        displayResults();
        btnPreviousQuestion.setEnabled(questionIndex > 0);
        btnNextQuestion.setEnabled(questionIndex < parent.quiz.getQuestions().size() - 1);
    }

    private void viewChart()
    {
        HashMap<String, Integer> answerCounts = parent.question.calcAnswerCounts(parent.quiz.getResponses(), questionIndex);
        if(answerCounts.isEmpty())
        {
            Toast.makeText(parent, R.string.no_chart_data_available, Toast.LENGTH_SHORT).show();
        }
        else
        {
            parent.vfQuizzes.setDisplayedChild(7);
            DataCharts.drawBarChart(bcResults, answerCounts);
            DataCharts.drawPieChart(pcResults, answerCounts);
        }
    }

    protected void displayResults(List<Quiz> quizzes)
    {
        if(parent.quiz == null)
        {
            return;
        }
        for(Quiz quiz : quizzes)
        {
            if(quiz.getKey().equals(parent.quiz.getKey()))
            {
                parent.quiz = quiz;
                displayResults();
                break;
            }
        }
    }

    @SuppressWarnings("unchecked")
    protected void displayResults()
    {
        boolean noResponses = parent.quiz.getResponses() == null || parent.quiz.getResponses().size() == 0;
        tvQuizTitle.setText(parent.quiz.getTitle());
        tvBestScore.setText(noResponses ? parent.getString(R.string.not_yet_available) : parent.quiz.writeBestScore());
        tvWorstScore.setText(noResponses ? parent.getString(R.string.not_yet_available) : parent.quiz.writeWorstScore());
        tvAverageScore.setText(noResponses ? parent.getString(R.string.not_yet_available) : parent.quiz.writeAverageScore());
        if(parent.quiz.getResponses() != null)
        {
            spnStudentA.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, parent.quiz.getResponders()));
        }
        if(parent.question != null)
        {
            tvQuestion.setText(parent.question.getQuestion());
            tvResults.setText(parent.question.writeResults(parent.quiz.getResponses(), questionIndex));
            tvStudentAnswers.setText("");
            spnStudentB.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, parent.quiz.getResponders(questionIndex)));
            HashMap<String, Integer> answerCounts = parent.question.calcAnswerCounts(parent.quiz.getResponses(), questionIndex);
            DataCharts.drawBarChart(bcResults, answerCounts);
            DataCharts.drawPieChart(pcResults, answerCounts);
        }
    }

}
