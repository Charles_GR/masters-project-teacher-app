package com.charles.smartteach.view.extras;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.charles.smartteach.R;
import com.charles.smartteach.model.runnable.SingleArgRunnable;

public abstract class PopupDialogs
{

    public static void showMessageDialog(final Context context, String title, String message)
    {
        Builder alertDialogBuilder = new Builder(context);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setPositiveButton("Ok", new OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });
        alertDialogBuilder.create().show();
    }

    public static void showInputDialog(final Context context, String title, String inputLabel, String positiveButtonText, String negativeButtonText, final SingleArgRunnable<String> onPositiveButtonPressed)
    {
        Builder alertDialogBuilder = new Builder(context);
        alertDialogBuilder.setTitle(title);
        View inputView = LayoutInflater.from(context).inflate(R.layout.layout_input_popup, null);
        alertDialogBuilder.setView(inputView);
        TextView tvInputLabel = (TextView)inputView.findViewById(R.id.tvInputLabel);
        final EditText etInputValue = (EditText)inputView.findViewById(R.id.etInputValue);
        tvInputLabel.setText(inputLabel);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(positiveButtonText, new OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                onPositiveButtonPressed.run(etInputValue.getText().toString());
            }
        });
        alertDialogBuilder.setNegativeButton(negativeButtonText, new OnClickListener()
        {
            public void onClick(DialogInterface dialog,int id)
            {
                dialog.cancel();
            }
       });
        alertDialogBuilder.create().show();
    }

}
