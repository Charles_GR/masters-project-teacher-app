package com.charles.smartteach.view.views.students.accounts;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import com.charles.smartteach.R;
import com.charles.smartteach.model.adapters.students.StudentAdapter;
import com.charles.smartteach.model.database.students.User;
import com.charles.smartteach.model.runnable.ListRunnable;
import com.charles.smartteach.model.runnable.SingleArgRunnable;
import com.charles.smartteach.processing.database.StudentData;
import java.util.List;

public class StudentAccountListView implements OnClickListener, OnItemSelectedListener, TextWatcher
{

    private StudentAccountsActivity parent;
    private Button btnCreateAccount;
    private Button btnEditAccount;
    private EditText etSearchQuery;
    private ListView lvAccounts;
    private ProgressBar pbAccountList;
    private Spinner spnSortField;
    private Spinner spnSortOrdering;
    private Spinner spnSearchField;
    private TextView tvAccounts;
    private TextView tvNoAccountsYet;
    private TextView tvAccountListError;

    public StudentAccountListView(StudentAccountsActivity parent)
    {
        this.parent = parent;
        btnCreateAccount = (Button)parent.findViewById(R.id.btnCreateAccount);
        btnEditAccount = (Button)parent.findViewById(R.id.btnEditAccount);
        etSearchQuery = (EditText)parent.findViewById(R.id.etSearchQuery);
        lvAccounts = (ListView)parent.findViewById(R.id.lvAccounts);
        pbAccountList = (ProgressBar)parent.findViewById(R.id.pbAccountList);
        spnSortField = (Spinner)parent.findViewById(R.id.spnSortField);
        spnSortOrdering = (Spinner)parent.findViewById(R.id.spnSortOrdering);
        spnSearchField = (Spinner)parent.findViewById(R.id.spnSearchField);
        tvAccounts = (TextView)parent.findViewById(R.id.tvAccounts);
        tvNoAccountsYet = (TextView)parent.findViewById(R.id.tvNoAccountsYet);
        tvAccountListError = (TextView)parent.findViewById(R.id.tvAccountListError);
        btnCreateAccount.setOnClickListener(this);
        btnEditAccount.setOnClickListener(this);
        etSearchQuery.addTextChangedListener(this);
        spnSortField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Full Name", "Username", "Email"}));
        spnSortField.setOnItemSelectedListener(this);
        spnSortOrdering.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Ascending", "Descending"}));
        spnSortOrdering.setOnItemSelectedListener(this);
        spnSearchField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Full Name", "Username", "Email"}));
        spnSearchField.setOnItemSelectedListener(this);
        StudentData.getStudents(getStudentsOnSuccess, getStudentsOnError);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnCreateAccount:
                tvAccounts.setError(null);
                parent.studentAccountFormView.createAccount();
                parent.vfAccounts.setDisplayedChild(1);
                break;
            case R.id.btnEditAccount:
                editAccount();
                break;

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        searchAndSortAccounts();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        searchAndSortAccounts();
    }

    @Override
    public void afterTextChanged(Editable s)
    {

    }

    private void searchAndSortAccounts()
    {
        lvAccounts.clearChoices();
        StudentAdapter studentAdapter = (StudentAdapter)lvAccounts.getAdapter();
        if(studentAdapter != null)
        {
            studentAdapter.searchItems(spnSearchField.getSelectedItem().toString(), etSearchQuery.getText().toString());
            studentAdapter.sortItems(spnSortField.getSelectedItem().toString(), spnSortOrdering.getSelectedItem().toString());
        }
    }

    private void editAccount()
    {
        tvAccounts.setError(null);
        if(lvAccounts.getCheckedItemCount() == 0)
        {
            tvAccounts.requestFocus();
            tvAccounts.setError(parent.getString(R.string.no_account_selected));
            return;
        }
        parent.student = (User)lvAccounts.getItemAtPosition(lvAccounts.getCheckedItemPosition());
        parent.studentAccountFormView.editAccount();
        parent.vfAccounts.setDisplayedChild(1);
    }

    private ListRunnable<User> getStudentsOnSuccess = new ListRunnable<User>()
    {
        @Override
        public void run(List<User> students)
        {
            lvAccounts.setAdapter(new StudentAdapter(parent, students));
            tvNoAccountsYet.setVisibility(students.isEmpty() ? View.VISIBLE : View.GONE);
            tvAccountListError.setVisibility(View.GONE);
            pbAccountList.setVisibility(View.GONE);
            btnCreateAccount.setEnabled(true);
            btnEditAccount.setEnabled(true);
            searchAndSortAccounts();
        }
    };

    private SingleArgRunnable<String> getStudentsOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            lvAccounts.setAdapter(new StudentAdapter(parent));
            tvNoAccountsYet.setVisibility(View.GONE);
            tvAccountListError.setText(text);
            tvAccountListError.setVisibility(View.VISIBLE);
            pbAccountList.setVisibility(View.GONE);
            btnCreateAccount.setEnabled(true);
            btnEditAccount.setEnabled(true);
        }
    };

}
