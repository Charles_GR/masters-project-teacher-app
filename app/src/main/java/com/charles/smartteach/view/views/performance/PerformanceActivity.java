package com.charles.smartteach.view.views.performance;

import android.os.Bundle;
import android.view.View;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import com.charles.smartteach.R;
import com.charles.smartteach.model.database.performance.Performance;
import com.charles.smartteach.model.database.students.Course;
import com.charles.smartteach.model.database.students.User;
import com.charles.smartteach.model.runnable.*;
import com.charles.smartteach.processing.database.*;
import com.charles.smartteach.view.templates.AppNavActivity;
import com.firebase.client.FirebaseError;
import java.util.*;

public class PerformanceActivity extends AppNavActivity implements OnItemSelectedListener
{

    private Spinner spnCourse;
    private Spinner spnTimePeriod;
    private Spinner spnStudent;
    private TextView tvBestStudents;
    private TextView tvWorstStudents;
    private TextView tvAverageScore;
    private TextView tvStudentScore;
    private TextView tvStudentEngagement;
    private TextView tvErrorMessage;

    private HashMap<String, Performance> performances;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_performance);
        spnCourse = (Spinner)findViewById(R.id.spnCourse);
        spnTimePeriod = (Spinner)findViewById(R.id.spnTimePeriod);
        spnStudent = (Spinner)findViewById(R.id.spnStudent);
        tvBestStudents = (TextView)findViewById(R.id.tvBestStudentsValue);
        tvWorstStudents = (TextView)findViewById(R.id.tvWorstStudentsValue);
        tvAverageScore = (TextView)findViewById(R.id.tvAverageScoreValue);
        tvStudentScore = (TextView)findViewById(R.id.tvStudentScoreValue);
        tvStudentEngagement = (TextView)findViewById(R.id.tvStudentEngagementValue);
        tvErrorMessage = (TextView)findViewById(R.id.tvErrorMessage);
        spnCourse.setOnItemSelectedListener(this);
        spnTimePeriod.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_item, new String[]{"All Time", "Past Year", "Past 6 Months", "Past 3 Months", "Past Month", "Past 2 Weeks", "Past Week", "Past 3 Days", "Past Day"}));
        spnTimePeriod.setOnItemSelectedListener(this);
        spnStudent.setOnItemSelectedListener(this);
        tvActionBarTitle.setText(R.string.performance_title);
        tvActionBarCourse.setVisibility(View.GONE);
        CourseData.getTeacherCourses(getCoursesOnSuccess, getCoursesOnError);
    }

    @Override
    protected void onUpButtonPressed()
    {
        goToLobby();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        switch(parent.getId())
        {
            case R.id.spnCourse:
                tvStudentScore.setText("");
                tvStudentEngagement.setText("");
                if(spnTimePeriod.getSelectedItem() != null)
                {
                    CourseData.getCourseStudents(spnCourse.getSelectedItem().toString(), getCourseStudentsOnSuccess, getCourseStudentsOnError);
                }
                break;
            case R.id.spnTimePeriod:
                if(spnCourse.getSelectedItem() != null)
                {
                    PerformanceData.getPerformances(spnCourse.getSelectedItem().toString(), spnTimePeriod.getSelectedItem().toString(), getPerformancesOnSuccess, getPerformancesOnError);
                }
                break;
            case R.id.spnStudent:
                Performance performance = performances.get(spnStudent.getSelectedItem().toString());
                tvStudentScore.setText(performance == null ? getString(R.string.not_yet_available) : performance.writeScore());
                PerformanceData.getStudentEngagement(spnCourse.getSelectedItem().toString(), spnStudent.getSelectedItem().toString(), getStudentEngagementOnSuccess, getStudentEngagementOnError);
                break;
        }
    }

    private ListRunnable<Course> getCoursesOnSuccess = new ListRunnable<Course>()
    {
        @Override
        public void run(List<Course> courses)
        {
            spnCourse.setAdapter(new ArrayAdapter<>(PerformanceActivity.this, R.layout.spinner_item, courses));
        }
    };

    private SingleArgRunnable<FirebaseError> getCoursesOnError = new SingleArgRunnable<FirebaseError>()
    {
        @Override
        public void run(FirebaseError firebaseError)
        {
            tvErrorMessage.setText(firebaseError.getMessage());
            tvErrorMessage.setVisibility(View.VISIBLE);
        }
    };

    private ListRunnable<User> getCourseStudentsOnSuccess = new ListRunnable<User>()
    {
        @Override
        public void run(List<User> users)
        {
            spnStudent.setAdapter(new ArrayAdapter<>(PerformanceActivity.this, R.layout.spinner_item, users));
            PerformanceData.getPerformances(spnCourse.getSelectedItem().toString(), spnTimePeriod.getSelectedItem().toString(), getPerformancesOnSuccess, getPerformancesOnError);
        }
    };

    private SingleArgRunnable<String> getCourseStudentsOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            tvErrorMessage.setText(text);
            tvErrorMessage.setVisibility(View.VISIBLE);
        }
    };

    private HashMapRunnable<String, Performance> getPerformancesOnSuccess = new HashMapRunnable<String, Performance>()
    {
        @Override
        public void run(HashMap<String, Performance> performances)
        {
            PerformanceActivity.this.performances = performances;
            tvBestStudents.setText(Performance.performancesToString(Performance.getBestPerformances(performances)));
            tvWorstStudents.setText(Performance.performancesToString(Performance.getWorstPerformances(performances)));
            tvAverageScore.setText(Performance.writeAverageScore(performances));
            if(spnStudent.getSelectedItem() == null)
            {
                tvStudentScore.setText("");
                tvStudentEngagement.setText("");
            }
            else
            {
                Performance performance = performances.get(spnStudent.getSelectedItem().toString());
                tvStudentScore.setText(performance == null ? getString(R.string.not_yet_available) : performance.writeScore());
            }
        }
    };

    private SingleArgRunnable<String> getPerformancesOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            tvErrorMessage.setText(text);
            tvErrorMessage.setVisibility(View.VISIBLE);
        }
    };

    private SingleArgRunnable<String> getStudentEngagementOnSuccess = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            tvStudentEngagement.setText(text);
        }
    };

    private SingleArgRunnable<String> getStudentEngagementOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            tvStudentEngagement.setText(R.string.not_yet_available);
        }
    };

}