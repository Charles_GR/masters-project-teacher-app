package com.charles.smartteach.view.views.classroom.questions;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.smartteach.R;
import com.charles.smartteach.model.database.classroom.questions.*;
import com.charles.smartteach.view.templates.AppActivity;
import com.charles.smartteach.view.views.classroom.ClassroomActivity;

public class ClassroomQuestionsActivity extends AppActivity implements OnClickListener
{

    private static ClassroomQuestionsActivity instance;

    protected ClassroomQuestionsListView classroomQuestionsListView;
    protected ClassroomQuestionFormView classroomQuestionFormView;
    protected ClassroomQuestionResultsView classroomQuestionResultsView;
    protected ViewFlipper vfQuestions;

    protected Question question;

    public static ClassroomQuestionsActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(!checkCourseIsSelected())
        {
            return;
        }
        instance = this;
        setContentView(R.layout.activity_classroom_questions);
        classroomQuestionsListView = new ClassroomQuestionsListView(this);
        classroomQuestionFormView = new ClassroomQuestionFormView(this);
        classroomQuestionResultsView = new ClassroomQuestionResultsView(this);
        vfQuestions = (ViewFlipper)findViewById(R.id.vfQuestion);
    }

    @Override
    public void onClick(View v)
    {
        ClassroomActivity.getInstance().onClick(v);
    }

    public void onUpButtonPressed()
    {
        switch(vfQuestions.getDisplayedChild())
        {
            case 0:
                goToLobby();
                break;
            case 1:
                vfQuestions.setDisplayedChild(0);
                break;
            case 2:
                vfQuestions.setDisplayedChild(1);
                break;
            case 3:
                vfQuestions.setDisplayedChild(0);
                break;
            case 4:
                vfQuestions.setDisplayedChild(3);
                break;
        }
    }

}
