package com.charles.smartteach.view.views.students.accounts;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.smartteach.R;
import com.charles.smartteach.model.database.students.User;
import com.charles.smartteach.view.templates.AppActivity;
import com.charles.smartteach.view.views.students.StudentActivity;

public class StudentAccountsActivity extends AppActivity implements OnClickListener
{

    private static StudentAccountsActivity instance;

    protected StudentAccountListView studentAccountListView;
    protected StudentAccountFormView studentAccountFormView;
    protected ViewFlipper vfAccounts;

    protected User student;
    protected String currentUsername;

    public static StudentAccountsActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        instance = this;
        setContentView(R.layout.activity_student_accounts);
        vfAccounts = (ViewFlipper)findViewById(R.id.vfAccounts);
        studentAccountListView = new StudentAccountListView(this);
        studentAccountFormView = new StudentAccountFormView(this);
    }

    @Override
    public void onClick(View v)
    {
        StudentActivity.getInstance().onClick(v);
    }

    public void onUpButtonPressed()
    {
        switch(vfAccounts.getDisplayedChild())
        {
            case 0:
                goToLobby();
                break;
            case 1:
                vfAccounts.setDisplayedChild(0);
                break;
        }
    }

}
