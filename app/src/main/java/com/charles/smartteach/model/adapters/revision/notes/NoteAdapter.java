package com.charles.smartteach.model.adapters.revision.notes;

import android.content.Context;
import android.view.*;
import android.widget.TextView;
import com.charles.smartteach.R;
import com.charles.smartteach.model.adapters.FilterAdapter;
import com.charles.smartteach.model.database.revision.notes.Note;
import java.text.SimpleDateFormat;
import java.util.*;

public class NoteAdapter extends FilterAdapter<Note>
{

    public NoteAdapter(Context context, List<Note> notes)
    {
        super(context, R.layout.list_item_note, notes);
        setComparator(new NoteComparator());
    }

    public NoteAdapter(Context context)
    {
        super(context, R.layout.list_item_note, new ArrayList<Note>());
        setComparator(new NoteComparator());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_note, parent, false);
        }
        Note note = getItem(position);
        TextView tvTitle = (TextView)convertView.findViewById(R.id.tvTitleValue);
        TextView tvAuthor = (TextView)convertView.findViewById(R.id.tvAuthorValue);
        TextView tvDateCreated = (TextView)convertView.findViewById(R.id.tvDateCreatedValue);
        tvTitle.setText(note.getTitle());
        tvAuthor.setText(note.getAuthor());
        tvDateCreated.setText(SimpleDateFormat.getDateTimeInstance().format(note.getDateCreated()));
        return convertView;
    }

    @Override
    protected void performSearch(String searchField, String searchQuery)
    {
        for(Note note : items)
        {
            switch(searchField)
            {
                case "Title":
                    if(note.getTitle().contains(searchQuery))
                    {
                        filteredItems.add(note);
                    }
                    break;
                case "Author":
                    if(note.getAuthor().contains(searchQuery))
                    {
                        filteredItems.add(note);
                    }
                    break;
            }
        }
    }

    protected class NoteComparator extends ItemComparator
    {

        @Override
        public int compare(Note noteA, Note noteB)
        {
            int compare = 0;
            switch(sortField)
            {
                case "Title":
                    compare = noteA.getTitle().compareTo(noteB.getTitle());
                    break;
                case "Author":
                    compare = noteA.getAuthor().compareTo(noteB.getAuthor());
                    break;
                case "Date Created":
                    compare = noteA.getDateCreated().compareTo(noteB.getDateCreated());
                    break;
                case "# Comments":
                    compare = Integer.valueOf(noteA.getComments().size()).compareTo(noteB.getComments().size());
                    break;
            }
            return sortOrdering.equals("Ascending") ? compare : -1 * compare;
        }

    }

}