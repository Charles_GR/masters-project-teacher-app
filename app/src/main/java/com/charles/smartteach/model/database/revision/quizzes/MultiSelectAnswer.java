package com.charles.smartteach.model.database.revision.quizzes;

public class MultiSelectAnswer extends Answer<String[]>
{

    public static final String ANSWER_TYPE_TAG = "Multi-Select";

    private String feedbackForSelect;
    private String feedbackForNotSelect;

    public MultiSelectAnswer(String answer, boolean correct, String feedbackForSelect, String feedbackForNotSelect)
    {
        super(ANSWER_TYPE_TAG, answer, correct);
        this.feedbackForSelect = feedbackForSelect;
        this.feedbackForNotSelect = feedbackForNotSelect;
    }

    public MultiSelectAnswer()
    {
        super(ANSWER_TYPE_TAG);
    }

    public String getFeedbackForSelect()
    {
        return feedbackForSelect;
    }

    public String getFeedbackForNotSelect()
    {
        return feedbackForNotSelect;
    }

    public void setFeedback(String... feedback)
    {
        this.feedbackForSelect = feedback[0];
        this.feedbackForNotSelect = feedback[1];
    }

    public void putFeedbackFromArray(String... feedback)
    {
        setFeedback(feedback);
    }

    @Override
    public String toString()
    {
        return super.toString() + "\n\nFeedback For Select: " + feedbackForSelect + "\n\nFeedback For Not Select: " + feedbackForNotSelect;
    }

}
