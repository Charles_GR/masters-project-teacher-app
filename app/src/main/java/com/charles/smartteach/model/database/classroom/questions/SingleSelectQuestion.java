package com.charles.smartteach.model.database.classroom.questions;

import android.content.Context;
import com.charles.smartteach.R;
import java.util.ArrayList;
import java.util.HashMap;

public class SingleSelectQuestion extends SelectQuestion<String>
{

    public static final String QUESTION_TYPE_TAG = "Single-Select";

    public SingleSelectQuestion()
    {
        super(QUESTION_TYPE_TAG);
    }

    public SingleSelectQuestion(SingleSelectQuestion singleSelectQuestion)
    {
        super(singleSelectQuestion);
        answers = new ArrayList<>(singleSelectQuestion.getAnswers());
    }

    @Override
    public HashMap<String, Integer> calcAnswerCounts()
    {
        HashMap<String, Integer> answerCounts = new HashMap<>();
        for(Response<String> response : responses.values())
        {
            String answer = response.getAnswers();
            answerCounts.put(answer, answerCounts.containsKey(answer) ? answerCounts.get(answer) + 1 : 1);
        }
        return answerCounts;
    }

    @Override
    public String checkAnswers(Context context)
    {
        String errorMessage = super.checkAnswers(context);
        if(errorMessage == null)
        {
            if(numAnswersCorrect() != 1)
            {
                return context.getString(R.string.single_select_correct_answer);
            }
        }
        return errorMessage;
    }

}
