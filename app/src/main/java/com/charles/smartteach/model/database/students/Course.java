package com.charles.smartteach.model.database.students;

import com.charles.smartteach.processing.database.LoginData;
import java.util.HashMap;

public class Course implements Comparable<Course>
{

    private String code;
    private String title;
    private String teacher;
    private HashMap<String, String> students;

    public Course(String code, String title)
    {
        this.code = code;
        this.title = title;
        teacher = LoginData.getUsername();
        students = new HashMap<>();
    }

    public Course()
    {
        students = new HashMap<>();
    }

    public String getCode()
    {
        return code;
    }

    public String getTitle()
    {
        return title;
    }

    public String getTeacher()
    {
        return teacher;
    }

    public HashMap<String, String> getStudents()
    {
        return students;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public void setTeacher(String teacher)
    {
        this.teacher = teacher;
    }

    @Override
    public String toString()
    {
        return code;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj instanceof Course)
        {
            Course course = (Course)obj;
            return course.getCode().equals(code);
        }
        return false;
    }

    @Override
    public int compareTo(Course otherCourse)
    {
        return code.compareTo(otherCourse.code);
    }

}
