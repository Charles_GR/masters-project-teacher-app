package com.charles.smartteach.model.database.revision.quizzes;

import android.content.Context;
import com.charles.smartteach.R;
import java.util.*;

public abstract class SelectQuestion extends Question
{

    protected List<Answer> answers;

    public SelectQuestion(String type)
    {
        super(type);
        answers = new ArrayList<>();
    }

    public List<Answer> getAnswers()
    {
        return answers;
    }

    public void setAnswers(List<Answer> answers)
    {
        this.answers = answers;
    }

    public abstract boolean addAnswer(String answer, boolean correct, String... feedback);

    public void removeAnswer(String answer)
    {
        Iterator<Answer> iterator = answers.iterator();
        while(iterator.hasNext())
        {
            if(iterator.next().getAnswer().equals(answer))
            {
                iterator.remove();
                break;
            }
        }
    }

    protected boolean containsAnswer(String answerText)
    {
        for(Answer answer : answers)
        {
            if(answer.getAnswer().equals(answerText))
            {
                return true;
            }
        }
        return false;
    }

    public String checkAnswers(Context context)
    {
        if(answers.size() < 2)
        {
            return context.getString(R.string.question_needs_answers);
        }
        return null;
    }

    protected int numAnswersCorrect()
    {
        int correctCount = 0;
        for(Answer answer : answers)
        {
            if(answer.getCorrect())
            {
                correctCount++;
            }
        }
        return correctCount;
    }

    protected int numAnswersIncorrect()
    {
        int incorrectCount = 0;
        for(Answer answer : answers)
        {
            if(!answer.getCorrect())
            {
                incorrectCount++;
            }
        }
        return incorrectCount;
    }

}