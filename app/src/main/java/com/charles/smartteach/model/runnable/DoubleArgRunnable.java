package com.charles.smartteach.model.runnable;

public interface DoubleArgRunnable<Ta, Tb>
{

    void run(Ta argA, Tb argB);

}

