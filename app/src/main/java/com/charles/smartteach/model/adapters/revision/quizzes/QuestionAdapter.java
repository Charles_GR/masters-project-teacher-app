package com.charles.smartteach.model.adapters.revision.quizzes;

import android.content.Context;
import android.view.*;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.charles.smartteach.R;
import com.charles.smartteach.model.database.revision.quizzes.Question;
import java.util.List;

public class QuestionAdapter extends ArrayAdapter<Question>
{

    public QuestionAdapter(Context context, List<Question> questions)
    {
        super(context, R.layout.list_item_rev_question, questions);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_rev_question, parent, false);
        }
        Question question = getItem(position);
        TextView tvQuestion = (TextView)convertView.findViewById(R.id.tvQuestion);
        tvQuestion.setText(question.getQuestion());
        return convertView;
    }

}

