package com.charles.smartteach.model.adapters.students;

import android.content.Context;
import android.view.*;
import android.widget.TextView;
import com.charles.smartteach.R;
import com.charles.smartteach.model.adapters.FilterAdapter;
import com.charles.smartteach.model.database.students.Course;
import java.util.*;

public class CourseAdapter extends FilterAdapter<Course>
{

    public CourseAdapter(Context context, List<Course> courses)
    {
        super(context, R.layout.list_item_course, courses);
        setComparator(new CourseComparator());
    }

    public CourseAdapter(Context context)
    {
        super(context, R.layout.list_item_course, new ArrayList<Course>());
        setComparator(new CourseComparator());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_course, parent, false);
        }
        Course course = getItem(position);
        TextView tvCode = (TextView)convertView.findViewById(R.id.tvCodeValue);
        TextView tvTitle = (TextView)convertView.findViewById(R.id.tvTitleValue);
        TextView tvStudents = (TextView)convertView.findViewById(R.id.tvStudentsValue);
        tvCode.setText(course.getCode());
        tvTitle.setText(course.getTitle());
        tvStudents.setText(String.valueOf(course.getStudents().size()));
        return convertView;
    }

    @Override
    protected void performSearch(String searchField, String searchQuery)
    {
        for(Course course : items)
        {
            switch(searchField)
            {
                case "Code":
                    if(course.getCode().contains(searchQuery))
                    {
                        filteredItems.add(course);
                    }
                    break;
                case "Title":
                    if(course.getTitle().contains(searchQuery))
                    {
                        filteredItems.add(course);
                    }
                    break;
            }
        }
    }

    private class CourseComparator extends ItemComparator
    {

        @Override
        public int compare(Course courseA, Course courseB)
        {
            int compare = 0;
            switch(sortField)
            {
                case "Code":
                    compare = courseA.getCode().compareTo(courseB.getCode());
                    break;
                case "Title":
                    compare = courseA.getTitle().compareTo(courseB.getTitle());
                    break;
                case "# Students":
                    compare = Integer.valueOf(courseA.getStudents().size()).compareTo(courseB.getStudents().size());
                    break;
            }
            return sortOrdering.equals("Ascending") ? compare : -1 * compare;
        }

    }

}