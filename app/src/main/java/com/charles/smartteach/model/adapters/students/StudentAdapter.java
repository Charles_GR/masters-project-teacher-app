package com.charles.smartteach.model.adapters.students;

import android.content.Context;
import android.view.*;
import android.widget.TextView;
import com.charles.smartteach.R;
import com.charles.smartteach.model.adapters.FilterAdapter;
import com.charles.smartteach.model.database.students.User;
import java.util.*;

public class StudentAdapter extends FilterAdapter<User>
{

    public StudentAdapter(Context context, List<User> students)
    {
        super(context, R.layout.list_item_student, students);
        setComparator(new StudentComparator());
    }

    public StudentAdapter(Context context)
    {
        super(context, R.layout.list_item_student, new ArrayList<User>());
        setComparator(new StudentComparator());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_student, parent, false);
        }
        User student = getItem(position);
        TextView tvFullName = (TextView)convertView.findViewById(R.id.tvFullNameValue);
        TextView tvUsername = (TextView)convertView.findViewById(R.id.tvUsernameValue);
        TextView tvEmail = (TextView)convertView.findViewById(R.id.tvEmailValue);
        tvFullName.setText(student.makeFullName());
        tvUsername.setText(student.getUsername());
        tvEmail.setText(student.getEmail());
        return convertView;
    }

    @Override
    protected void performSearch(String searchField, String searchQuery)
    {
        for(User student : items)
        {
            switch(searchField)
            {
                case "Full Name":
                    if(student.makeFullName().contains(searchQuery))
                    {
                        filteredItems.add(student);
                    }
                    break;
                case "Username":
                    if(student.getUsername().contains(searchQuery))
                    {
                        filteredItems.add(student);
                    }
                    break;
                case "Email":
                    if(student.getEmail().contains(searchQuery))
                    {
                        filteredItems.add(student);
                    }
                    break;
            }
        }
    }

    private class StudentComparator extends ItemComparator
    {

        @Override
        public int compare(User studentA, User studentB)
        {
            int compare = 0;
            switch(sortField)
            {
                case "Full Name":
                    compare = studentA.makeFullName().compareTo(studentB.makeFullName());
                    break;
                case "Username":
                    compare = studentA.getUsername().compareTo(studentB.getUsername());
                    break;
                case "Email":
                    compare = studentA.getEmail().compareTo(studentB.getEmail());
                    break;
            }
            return sortOrdering.equals("Ascending") ? compare : -1 * compare;
        }

    }

}