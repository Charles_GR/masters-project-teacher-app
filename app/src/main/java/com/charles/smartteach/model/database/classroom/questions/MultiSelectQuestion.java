package com.charles.smartteach.model.database.classroom.questions;

import android.content.Context;
import com.charles.smartteach.R;
import java.util.*;

public class MultiSelectQuestion extends SelectQuestion<List<String>>
{

    public static final String QUESTION_TYPE_TAG = "Multi-Select";

    public MultiSelectQuestion()
    {
        super(QUESTION_TYPE_TAG);
    }

    public MultiSelectQuestion(MultiSelectQuestion multiSelectQuestion)
    {
        super(QUESTION_TYPE_TAG);
        answers = new ArrayList<>(multiSelectQuestion.getAnswers());
    }

    @Override
    public HashMap<String, Integer> calcAnswerCounts()
    {
        HashMap<String, Integer> answerCounts = new HashMap<>();
        for(Response<List<String>> response : responses.values())
        {
            for(String answer : response.getAnswers())
            {
                answerCounts.put(answer, answerCounts.containsKey(answer) ? answerCounts.get(answer) + 1 : 1);
            }
        }
        return answerCounts;
    }

    @Override
    public String checkAnswers(Context context)
    {
        String errorMessage = super.checkAnswers(context);
        if(errorMessage == null)
        {
            if(numAnswersCorrect() == 0)
            {
                return context.getString(R.string.multi_select_correct_answer);
            }
            if(numAnswersIncorrect() == 0)
            {
                return context.getString(R.string.multi_select_incorrect_answer);
            }
        }
        return errorMessage;
    }

}
