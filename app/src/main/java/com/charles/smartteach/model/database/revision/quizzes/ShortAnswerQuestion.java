package com.charles.smartteach.model.database.revision.quizzes;

import java.util.*;

public class ShortAnswerQuestion extends Question
{

    public static final String QUESTION_TYPE_TAG = "Short Answer";

    private String correctAnswer;

    private HashMap<String, Feedback> feedback;

    public ShortAnswerQuestion()
    {
        super(QUESTION_TYPE_TAG);
        feedback = new HashMap<>();
    }

    public String getCorrectAnswer()
    {
        return correctAnswer;
    }

    public HashMap<String, Feedback> getFeedback()
    {
        return feedback;
    }

    public List<Feedback> createFeedbackList()
    {
        return new ArrayList<>(feedback.values());
    }

    public void setCorrectAnswer(String correctAnswer)
    {
        this.correctAnswer = correctAnswer;
    }

    @Override
    public HashMap<String, Integer> calcAnswerCounts(HashMap<String, Response> responses, int questionIndex)
    {
        HashMap<String, Integer> answerCounts = new HashMap<>();
        for(Response response : responses.values())
        {
            if(questionIndex < response.getAnswers().size())
            {
                String answer = (String)response.getAnswers().get(questionIndex);
                if(answer != null)
                {
                    answerCounts.put(answer, answerCounts.containsKey(answer) ? answerCounts.get(answer) + 1 : 1);
                }
            }
        }
        return answerCounts;
    }

    @Override
    public int calcPointsPossible()
    {
        return 1;
    }

    public boolean addFeedback(String answer, String feedbackValue)
    {
        if(!feedback.containsKey(answer))
        {
            feedback.put(answer, new Feedback(answer, feedbackValue));
            return true;
        }
        return false;
    }

    public void editFeedback(String oldAnswer, String newAnswer, String feedbackValue)
    {
        feedback.remove(oldAnswer);
        feedback.put(newAnswer, new Feedback(newAnswer, feedbackValue));
    }

    public void removeFeedback(String answer)
    {
        feedback.remove(answer);
    }

}
