package com.charles.smartteach.model.adapters.classroom.questions;

import android.content.Context;
import android.view.*;
import android.widget.TextView;
import com.charles.smartteach.R;
import com.charles.smartteach.model.adapters.FilterAdapter;
import com.charles.smartteach.model.database.classroom.questions.Question;
import java.text.SimpleDateFormat;
import java.util.List;

public class QuestionAdapter extends FilterAdapter<Question>
{

    public QuestionAdapter(Context context, List<Question> questions)
    {
        super(context, R.layout.list_item_class_question, questions);
        setComparator(new ClassroomQuestionComparator());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_class_question, parent, false);
        }
        Question question = getItem(position);
        TextView tvQuestion = (TextView)convertView.findViewById(R.id.tvQuestionValue);
        TextView tvDateCreated = (TextView)convertView.findViewById(R.id.tvDateCreatedValue);
        TextView tvResponses = (TextView)convertView.findViewById(R.id.tvResponsesValue);
        tvQuestion.setText(question.getQuestion());
        tvDateCreated.setText(SimpleDateFormat.getDateTimeInstance().format(question.getDateCreated()));
        tvResponses.setText(String.valueOf(question.getResponses().size()));
        return convertView;
    }

    @Override
    protected void performSearch(String searchField, String searchQuery)
    {
        for(Question question : items)
        {
            if(searchField.equals("Question"))
            {
                if(question.getQuestion().contains(searchQuery))
                {
                    filteredItems.add(question);
                }
            }
        }
    }

    protected class ClassroomQuestionComparator extends ItemComparator
    {

        @Override
        public int compare(Question questionA, Question questionB)
        {
            int compare = 0;
            switch(sortField)
            {
                case "Question":
                    compare = questionA.getQuestion().compareTo(questionB.getQuestion());
                    break;
                case "Date Created":
                    compare = questionA.getDateCreated().compareTo(questionB.getDateCreated());
                    break;
                case "# Responses":
                    compare = Integer.valueOf(questionA.getResponses().size()).compareTo(questionB.getResponses().size());
                    break;
            }
            return sortOrdering.equals("Ascending") ? compare : -1 * compare;
        }
    }

}

