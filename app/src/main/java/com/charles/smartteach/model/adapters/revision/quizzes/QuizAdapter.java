package com.charles.smartteach.model.adapters.revision.quizzes;

import android.content.Context;
import android.view.*;
import android.widget.TextView;
import com.charles.smartteach.R;
import com.charles.smartteach.model.adapters.FilterAdapter;
import com.charles.smartteach.model.database.revision.quizzes.Quiz;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class QuizAdapter extends FilterAdapter<Quiz>
{

    public QuizAdapter(Context context, List<Quiz> quizzes)
    {
        super(context, R.layout.list_item_quiz, quizzes);
        setComparator(new QuizComparator());
    }

    public QuizAdapter(Context context)
    {
        super(context, R.layout.list_item_quiz, new ArrayList<Quiz>());
        setComparator(new QuizComparator());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_quiz, parent, false);
        }
        Quiz quiz = getItem(position);
        TextView tvTitle = (TextView)convertView.findViewById(R.id.tvTitleValue);
        TextView tvDateCreated = (TextView)convertView.findViewById(R.id.tvDateCreatedValue);
        TextView tvResponses = (TextView)convertView.findViewById(R.id.tvResponsesValue);
        tvTitle.setText(quiz.getTitle());
        tvDateCreated.setText(SimpleDateFormat.getDateTimeInstance().format(quiz.getDateCreated()));
        tvResponses.setText(String.valueOf(quiz.getResponses() == null ? 0 : quiz.getResponses().size()));
        return convertView;
    }

    @Override
    protected void performSearch(String searchField, String searchQuery)
    {
        for(Quiz quiz : items)
        {
            if(searchField.equals("Title"))
            {
                if(quiz.getTitle().contains(searchQuery))
                {
                    filteredItems.add(quiz);
                }
            }
        }
    }

    protected class QuizComparator extends ItemComparator
    {

        @Override
        public int compare(Quiz quizA, Quiz quizB)
        {
            int compare = 0;
            switch(sortField)
            {
                case "Title":
                    compare = quizA.getTitle().compareTo(quizB.getTitle());
                    break;
                case "Date Created":
                    compare = quizA.getDateCreated().compareTo(quizB.getDateCreated());
                    break;
                case "# Responses":
                    compare = Integer.valueOf(quizA.getResponses().size()).compareTo(quizB.getResponses().size());
                    break;
            }
            return sortOrdering.equals("Ascending") ? compare : -1 * compare;
        }

    }

}
