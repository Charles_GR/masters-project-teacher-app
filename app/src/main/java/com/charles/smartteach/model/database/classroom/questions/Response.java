package com.charles.smartteach.model.database.classroom.questions;

import java.util.Iterator;
import java.util.List;

public class Response<AnswersType>
{

    private String student;
    private AnswersType answers;

    public Response()
    {

    }

    public String getStudent()
    {
        return student;
    }

    public AnswersType getAnswers()
    {
        return answers;
    }

    public void setStudent(String student)
    {
        this.student = student;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String toString()
    {
        if(answers instanceof String)
        {
            return (String)answers;
        }
        else if(answers instanceof Boolean)
        {
            return String.valueOf(answers);
        }
        else if(answers instanceof List)
        {
            String answerText = "";
            List<String> answerList = (List<String>)answers;
            Iterator<String> iterator = answerList.iterator();
            while(iterator.hasNext())
            {
                answerText += iterator.next();
                if(iterator.hasNext())
                {
                    answerText += ", ";
                }
            }
            return answerText;
        }
        return super.toString();
    }

}