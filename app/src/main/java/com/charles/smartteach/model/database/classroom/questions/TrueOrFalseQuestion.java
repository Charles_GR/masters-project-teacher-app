package com.charles.smartteach.model.database.classroom.questions;

import java.util.HashMap;

public class TrueOrFalseQuestion extends Question<Boolean>
{

    public static final String QUESTION_TYPE_TAG = "True Or False";

    private Boolean correctAnswer;

    public TrueOrFalseQuestion()
    {
        super(QUESTION_TYPE_TAG);
    }

    public TrueOrFalseQuestion(TrueOrFalseQuestion trueOrFalseQuestion)
    {
        super(trueOrFalseQuestion);
        correctAnswer = trueOrFalseQuestion.getCorrectAnswer();
    }

    public Boolean getCorrectAnswer()
    {
        return correctAnswer;
    }

    public void setCorrectAnswer(boolean correctAnswer)
    {
        this.correctAnswer = correctAnswer;
    }

    @Override
    public HashMap<String, Integer> calcAnswerCounts()
    {
        HashMap<String, Integer> answerCounts = new HashMap<>();
        for(Response<Boolean> response : responses.values())
        {
            String answer = response.getAnswers().toString();
            answerCounts.put(answer, answerCounts.containsKey(answer) ? answerCounts.get(answer) + 1 : 1);
        }
        return answerCounts;
    }

}

