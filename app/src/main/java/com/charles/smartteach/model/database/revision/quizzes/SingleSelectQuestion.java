package com.charles.smartteach.model.database.revision.quizzes;

import android.content.Context;
import com.charles.smartteach.R;
import java.util.HashMap;

public class SingleSelectQuestion extends SelectQuestion
{

    public static final String QUESTION_TYPE_TAG = "Single-Select";

    public SingleSelectQuestion()
    {
        super(QUESTION_TYPE_TAG);
    }

    @Override
    public HashMap<String, Integer> calcAnswerCounts(HashMap<String, Response> responses, int questionIndex)
    {
        HashMap<String, Integer> answerCounts = new HashMap<>();
        for(Response response : responses.values())
        {
            if(questionIndex < response.getAnswers().size())
            {
                String answer = response.getAnswers().get(questionIndex).toString();
                if(answer != null)
                {
                    answerCounts.put(answer, answerCounts.containsKey(answer) ? answerCounts.get(answer) + 1 : 1);
                }
            }
        }
        return answerCounts;
    }

    @Override
    public int calcPointsPossible()
    {
        return 1;
    }

    @Override
    public boolean addAnswer(String answer, boolean correct, String... feedback)
    {
        if(!containsAnswer(answer))
        {
            answers.add(new SingleSelectAnswer(answer, correct, feedback[0]));
            return true;
        }
        return false;
    }

    @Override
    public String checkAnswers(Context context)
    {
        String errorMessage = super.checkAnswers(context);
        if(errorMessage == null)
        {
            if(numAnswersCorrect() != 1)
            {
                return context.getString(R.string.single_select_correct_answer);
            }
        }
        return errorMessage;
    }

}
