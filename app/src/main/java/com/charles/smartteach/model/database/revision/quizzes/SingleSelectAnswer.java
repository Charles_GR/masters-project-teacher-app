package com.charles.smartteach.model.database.revision.quizzes;

public class SingleSelectAnswer extends Answer<String>
{

    public static final String ANSWER_TYPE_TAG = "Single-Select";

    private String feedback;

    public SingleSelectAnswer(String answer, boolean correct, String feedback)
    {
        super(ANSWER_TYPE_TAG, answer, correct);
        this.feedback = feedback;
    }

    public SingleSelectAnswer()
    {
        super(ANSWER_TYPE_TAG);
    }

    public String getFeedback()
    {
        return feedback;
    }

    public void setFeedback(String feedback)
    {
        this.feedback = feedback;
    }

    public void putFeedbackFromArray(String... feedback)
    {
        setFeedback(feedback[0]);
    }

    @Override
    public String toString()
    {
        return super.toString() + "\n\nFeedback For Select: " + feedback;
    }

}
