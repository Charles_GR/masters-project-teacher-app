package com.charles.smartteach.model.database.revision.quizzes;

import android.content.Context;
import com.charles.smartteach.R;
import java.util.*;

public class MultiSelectQuestion extends SelectQuestion
{

    public static final String QUESTION_TYPE_TAG = "Multi-Select";

    public MultiSelectQuestion()
    {
        super(QUESTION_TYPE_TAG);
    }

    @SuppressWarnings("unchecked")
    @Override
    public HashMap<String, Integer> calcAnswerCounts(HashMap<String, Response> responses, int questionIndex)
    {
        HashMap<String, Integer> answerCounts = new HashMap<>();
        for(Response response : responses.values())
        {
            if(questionIndex < response.getAnswers().size())
            {
                List<String> answers = (List<String>)response.getAnswers().get(questionIndex);
                if(answers != null)
                {
                    for(String answer : answers)
                    {
                        answerCounts.put(answer, answerCounts.containsKey(answer) ? answerCounts.get(answer) + 1 : 1);
                    }
                }
            }
        }
        return answerCounts;
    }

    @Override
    public int calcPointsPossible()
    {
        int pointsPossible = 0;
        for(Answer answer : answers)
        {
            if(answer.getCorrect())
            {
                pointsPossible++;
            }
        }
        return pointsPossible;
    }

    @Override
    public boolean addAnswer(String answer, boolean correct, String... feedback)
    {
        if(!containsAnswer(answer))
        {
            answers.add(new MultiSelectAnswer(answer, correct, feedback[0], feedback[1]));
            return true;
        }
        return false;
    }

    @Override
    public String checkAnswers(Context context)
    {
        String errorMessage = super.checkAnswers(context);
        if(errorMessage == null)
        {
            if(numAnswersCorrect() == 0)
            {
                return context.getString(R.string.multi_select_correct_answer);
            }
            if(numAnswersIncorrect() == 0)
            {
                return context.getString(R.string.multi_select_incorrect_answer);
            }
        }
        return errorMessage;
    }

}
