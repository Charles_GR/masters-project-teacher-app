package com.charles.smartteach.model.database.classroom.questions;

import java.util.*;

public class ShortAnswerQuestion extends Question<String>
{

    public static final String QUESTION_TYPE_TAG = "Short Answer";

    private String correctAnswer;

    public ShortAnswerQuestion()
    {
        super(QUESTION_TYPE_TAG);
    }

    public ShortAnswerQuestion(ShortAnswerQuestion shortAnswerQuestion)
    {
        super(shortAnswerQuestion);
        correctAnswer = shortAnswerQuestion.getCorrectAnswer();
    }

    public String getCorrectAnswer()
    {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer)
    {
        this.correctAnswer = correctAnswer;
    }

    @Override
    public HashMap<String, Integer> calcAnswerCounts()
    {
        HashMap<String, Integer> answerCounts = new HashMap<>();
        for(Response<String> response : responses.values())
        {
            String answer = response.getAnswers();
            answerCounts.put(answer, answerCounts.containsKey(answer) ? answerCounts.get(answer) + 1 : 1);
        }
        return answerCounts;
    }

    public List<String> getShortAnswers()
    {
        Set<String> shortAnswers = new TreeSet<>();
        for(Response<String> response : responses.values())
        {
            shortAnswers.add(response.getAnswers());
        }
        return new ArrayList<>(shortAnswers);
    }

}
