package com.charles.smartteach.model.database.revision.quizzes;

import java.util.HashMap;

public class TrueOrFalseQuestion extends Question
{

    public static final String QUESTION_TYPE_TAG = "True Or False";

    private Boolean correctAnswer;
    private String feedbackForTrue;
    private String feedbackForFalse;

    public TrueOrFalseQuestion()
    {
        super(QUESTION_TYPE_TAG);
    }

    public Boolean getCorrectAnswer()
    {
        return correctAnswer;
    }

    public String getFeedbackForTrue()
    {
        return feedbackForTrue;
    }

    public String getFeedbackForFalse()
    {
        return feedbackForFalse;
    }

    public void setCorrectAnswer(boolean correctAnswer)
    {
        this.correctAnswer = correctAnswer;
    }

    public void setFeedbackForTrue(String feedbackForTrue)
    {
        this.feedbackForTrue = feedbackForTrue;
    }

    public void setFeedbackForFalse(String feedbackForFalse)
    {
        this.feedbackForFalse = feedbackForFalse;
    }

    @Override
    public HashMap<String, Integer> calcAnswerCounts(HashMap<String, Response> responses, int questionIndex)
    {
        HashMap<String, Integer> answerCounts = new HashMap<>();
        for(Response response : responses.values())
        {
            if(questionIndex < response.getAnswers().size())
            {
                String answer = response.getAnswers().get(questionIndex).toString();
                if(answer != null)
                {
                    answerCounts.put(answer, answerCounts.containsKey(answer) ? answerCounts.get(answer) + 1 : 1);
                }
            }
        }
        return answerCounts;
    }

    @Override
    public int calcPointsPossible()
    {
        return 1;
    }

}

