package com.charles.smartteach.model.adapters.revision.quizzes;

import android.content.Context;
import android.view.*;
import android.widget.*;
import com.charles.smartteach.R;
import com.charles.smartteach.model.database.revision.quizzes.Feedback;
import java.util.ArrayList;
import java.util.List;

public class FeedbackAdapter extends ArrayAdapter<Feedback>
{

    public FeedbackAdapter(Context context, List<Feedback> feedbacks)
    {
        super(context, R.layout.list_item_feedback, feedbacks);
    }

    public FeedbackAdapter(Context context)
    {
        super(context, R.layout.list_item_feedback, new ArrayList<Feedback>());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_feedback, parent, false);
        }
        Feedback feedback = getItem(position);
        TextView tvAnswer = (TextView)convertView.findViewById(R.id.tvAnswer);
        tvAnswer.setText(feedback.getAnswer());
        return convertView;
    }

}
