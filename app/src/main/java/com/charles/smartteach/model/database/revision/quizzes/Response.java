package com.charles.smartteach.model.database.revision.quizzes;

import java.util.*;

public class Response implements Comparable<Response>
{

    private String student;
    private int points;
    private List answers;

    public Response(String student)
    {
        this.student = student;
        points = 0;
        answers = new ArrayList();
    }

    public Response()
    {
        points = 0;
        answers = new ArrayList();
    }

    public String getStudent()
    {
        return student;
    }

    public int getPoints()
    {
        return points;
    }

    public List getAnswers()
    {
        return answers;
    }

    @Override
    public int compareTo(Response otherResponse)
    {
        return Integer.valueOf(points).compareTo(otherResponse.getPoints());
    }

    @SuppressWarnings("unchecked")
    public String answersToString(int questionIndex)
    {
        Object answer = answers.get(questionIndex);
        if(answer instanceof String)
        {
            return (String)answer;
        }
        else if(answer instanceof Boolean)
        {
            return String.valueOf(answer);
        }
        else if(answer instanceof List)
        {
            String answerText = "";
            List<String> answerList = (List<String>)answer;
            Iterator<String> iterator = answerList.iterator();
            while(iterator.hasNext())
            {
                answerText += iterator.next();
                if(iterator.hasNext())
                {
                    answerText += ", ";
                }
            }
            return answerText;
        }
        return super.toString();
    }

}
