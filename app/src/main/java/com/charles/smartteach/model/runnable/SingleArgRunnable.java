package com.charles.smartteach.model.runnable;

public interface SingleArgRunnable<T>
{

    void run(T arg);

}
