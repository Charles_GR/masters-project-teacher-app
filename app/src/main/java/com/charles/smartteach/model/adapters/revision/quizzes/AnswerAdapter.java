package com.charles.smartteach.model.adapters.revision.quizzes;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.*;
import android.widget.*;
import com.charles.smartteach.R;
import com.charles.smartteach.model.database.revision.quizzes.Answer;
import java.util.ArrayList;
import java.util.List;

public class AnswerAdapter extends ArrayAdapter<Answer>
{

    private List<Answer> answers;

    public AnswerAdapter(Context context, List<Answer> answers)
    {
        super(context, R.layout.list_item_answer, answers);
        this.answers = answers;
    }

    public AnswerAdapter(Context context)
    {
        super(context, R.layout.list_item_answer, new ArrayList<Answer>());
        answers = new ArrayList<>();
    }

    public List<Answer> getAnswers()
    {
        return answers;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_answer, parent, false);
        }
        Answer answer = getItem(position);
        TextView tvAnswer = (TextView)convertView.findViewById(R.id.tvAnswer);
        ImageView ivIsCorrect = (ImageView)convertView.findViewById(R.id.ivIsCorrect);
        tvAnswer.setText(answer.getAnswer());
        Drawable drawable = ContextCompat.getDrawable(getContext(), answer.getCorrect() ? R.mipmap.correct : R.mipmap.incorrect);
        ivIsCorrect.setImageDrawable(drawable);
        return convertView;
    }

}
