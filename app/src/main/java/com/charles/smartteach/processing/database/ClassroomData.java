package com.charles.smartteach.processing.database;

import android.graphics.Bitmap;
import android.widget.ImageView;
import android.widget.TextView;
import com.charles.smartteach.model.database.classroom.comments.Comment;
import com.charles.smartteach.model.database.classroom.comments.CommentReply;
import com.charles.smartteach.model.database.classroom.questions.*;
import com.charles.smartteach.model.runnable.*;
import com.charles.smartteach.processing.media.ImageParser;
import com.firebase.client.*;
import java.io.FileNotFoundException;
import java.util.*;

public abstract class ClassroomData
{

    public static void getQuestions(final ListRunnable<Question> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getClassroomQuestionsRef().addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                List<Question> questions = new ArrayList<>();
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    switch(child.child("type").getValue(String.class))
                    {
                        case SingleSelectQuestion.QUESTION_TYPE_TAG:
                            questions.add(child.getValue(SingleSelectQuestion.class));
                            break;
                        case MultiSelectQuestion.QUESTION_TYPE_TAG:
                            questions.add(child.getValue(MultiSelectQuestion.class));
                            break;
                        case TrueOrFalseQuestion.QUESTION_TYPE_TAG:
                            questions.add(child.getValue(TrueOrFalseQuestion.class));
                            break;
                        case ShortAnswerQuestion.QUESTION_TYPE_TAG:
                            questions.add(child.getValue(ShortAnswerQuestion.class));
                            break;
                    }
                }
                onSuccess.run(questions);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void deleteShortAnswerResponses(final Question question, final String answer)
    {
        FirebaseData.getClassroomQuestionResponsesRef(question.getKey()).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    Response response = child.getValue(Response.class);
                    if(response.getAnswers().equals(answer))
                    {
                        child.getRef().removeValue();
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {

            }
        });
    }

    public static void getComments(final SingleArgRunnable<List<Comment>> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getClassroomCommentsRef().addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                List<Comment> comments = new ArrayList<>();
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    comments.add(child.getValue(Comment.class));
                }
                onSuccess.run(comments);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void postComment(final String text, String imageKey, final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        Firebase newCommentRef = FirebaseData.getClassroomCommentsRef().push();
        Comment comment = new Comment(newCommentRef.getKey(), text, imageKey);
        newCommentRef.setValue(comment, new Firebase.CompletionListener()
        {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase)
            {
                if(firebaseError == null)
                {
                    onSuccess.run();
                }
                else
                {
                    onError.run(firebaseError.getMessage());
                }
            }
        });
    }

    public static void postCommentReply(Comment comment, String text)
    {
        Firebase newCommentReplyRef = FirebaseData.getClassroomCommentRepliesRef(comment.getKey()).push();
        newCommentReplyRef.setValue(new CommentReply(newCommentReplyRef.getKey(), comment.getKey(), text));
    }

    public static String addImage(String path) throws FileNotFoundException
    {
        Firebase newImageRef = FirebaseData.getClassroomImagesRef().push();
        String encodedImage = ImageParser.encodeImage(path);
        newImageRef.setValue(encodedImage);
        return newImageRef.getKey();
    }

    public static void getImage(String key, final ImageView ivImage, final TextView tvError, final DoubleArgRunnable<Bitmap, ImageView> onSuccess, final DoubleArgRunnable<String, TextView> onError)
    {
        FirebaseData.getClassroomImageRef(key).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                String encodedImage = dataSnapshot.getValue(String.class);
                Bitmap bitmap = ImageParser.decodeImage(encodedImage);
                onSuccess.run(bitmap, ivImage);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage(), tvError);
            }
        });
    }

}