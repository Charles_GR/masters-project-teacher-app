package com.charles.smartteach.processing.database;

import com.charles.smartteach.model.database.performance.Performance;
import com.charles.smartteach.model.database.performance.Score;
import com.charles.smartteach.model.database.revision.quizzes.Quiz;
import com.charles.smartteach.model.runnable.HashMapRunnable;
import com.charles.smartteach.model.runnable.SingleArgRunnable;
import com.firebase.client.*;
import java.util.HashMap;
import java.util.Locale;

public abstract class PerformanceData
{

    public static void getPerformances(String courseCode, final String timePeriod, final HashMapRunnable<String, Performance> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getPerformancesRef(courseCode).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                HashMap<String, Performance> performances = new HashMap<>();
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    Performance performance = child.getValue(Performance.class);
                    performance.filterByTime(timePeriod);
                    performances.put(child.getKey(), performance);
                }
                onSuccess.run(performances);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void getStudentEngagement(String courseCode, final String student, final SingleArgRunnable<String> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getCourseRef(courseCode).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                int courseQuestionCount = 0;
                int studentQuestionCount = 0;
                courseQuestionCount += dataSnapshot.child("classroom").child("questions").getChildrenCount();
                for(DataSnapshot quizChild : dataSnapshot.child("revision").child("quizzes").getChildren())
                {
                    courseQuestionCount += quizChild.getValue(Quiz.class).getQuestions().size();
                }
                for(DataSnapshot scoreChild : dataSnapshot.child("performances").child(student).child("scores").getChildren())
                {
                    Score score = scoreChild.getValue(Score.class);
                    if(dataSnapshot.child("classroom").child("questions").hasChild(score.getContentKey()) ||
                        dataSnapshot.child("revision").child("quizzes").hasChild(score.getContentKey()))
                    {
                        studentQuestionCount += score.getQuestionCount();
                    }
                }
                onSuccess.run(calcFormattedPercentage(studentQuestionCount, courseQuestionCount));
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    private static String calcFormattedPercentage(int part, int whole)
    {
        if(whole == 0)
        {
            return "";
        }
        float percentage = Math.round(1000f * part / whole) / 10f;
        String percentageText = (percentage == (long)percentage) ? String.format(Locale.UK, "%d", (long)percentage) + "%"
                                                                 : String.format("%s", percentage) + "%";
        return part + " / " + whole + " = " + percentageText;
    }

}