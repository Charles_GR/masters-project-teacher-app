package com.charles.smartteach.processing.database;

import com.charles.smartteach.R;
import com.charles.smartteach.model.TeachApplication;
import com.charles.smartteach.model.database.classroom.questions.Response;
import com.charles.smartteach.model.database.performance.Performance;
import com.charles.smartteach.model.database.revision.notes.Note;
import com.charles.smartteach.model.database.students.Course;
import com.charles.smartteach.model.database.students.User;
import com.charles.smartteach.model.runnable.*;
import com.firebase.client.*;
import com.firebase.client.Firebase.CompletionListener;
import java.util.*;

public abstract class CourseData
{

    public static void getCourses(final ListRunnable<Course> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.coursesRef.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                List<Course> courses = new ArrayList<>();
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    courses.add(child.child("info").getValue(Course.class));
                }
                onSuccess.run(courses);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void addCourse(final String code, final String title, final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.coursesRef.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    if(child.child("info").getValue(Course.class).getCode().equals(code))
                    {
                        onError.run(TeachApplication.getInstance().getString(R.string.code_in_use));
                        return;
                    }
                }
                FirebaseData.getCourseInfoRef(code).setValue(new Course(code, title));
                onSuccess.run();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void editCourse(final String newCode, final String newTitle, final String currentCode, final Runnable onSuccess, final SingleArgRunnable<FirebaseError> onError)
    {
        FirebaseData.coursesRef.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                if(dataSnapshot.hasChild(newCode) && !newCode.equals(currentCode))
                {
                    onError.run(null);
                    return;
                }
                FirebaseData.coursesRef.child(currentCode).addListenerForSingleValueEvent(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(final DataSnapshot dataSnapshot)
                    {
                        final Course course = dataSnapshot.child("info").getValue(Course.class);
                        course.setCode(newCode);
                        course.setTitle(newTitle);
                        FirebaseData.getCourseRef(newCode).setValue(dataSnapshot.getValue(), new CompletionListener()
                        {
                            @Override
                            public void onComplete(FirebaseError firebaseError, Firebase firebase)
                            {
                                FirebaseData.getCourseRef(newCode).child("info").setValue(course);
                                FirebaseData.getCourseRef(currentCode).removeValue();
                                onSuccess.run();
                            }
                        });
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError)
                    {
                        onError.run(firebaseError);
                    }
                });
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError);
            }
        });
    }

    public static void getCourseStudents(final String courseCode, final ListRunnable<User> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getCourseInfoRef(courseCode).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                final List<String> courseStudentUsernames = new ArrayList<>();
                for(DataSnapshot child : dataSnapshot.child("students").getChildren())
                {
                    courseStudentUsernames.add(child.getValue(String.class));
                }
                FirebaseData.studentsRef.addListenerForSingleValueEvent(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        Map<String, User> students = new HashMap<>();
                        for(DataSnapshot child : dataSnapshot.getChildren())
                        {
                            User student = child.getValue(User.class);
                            students.put(student.getUsername(), student);
                        }
                        List<User> courseStudents = new ArrayList<>();
                        for(String username : courseStudentUsernames)
                        {
                            courseStudents.add(students.get(username));
                        }
                        onSuccess.run(courseStudents);
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError)
                    {
                        onError.run(firebaseError.getMessage());
                    }
                });
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void getNonCourseStudents(final String courseCode, final ListRunnable<User> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getCourseInfoRef(courseCode).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                final List<String> courseStudentUsernames = new ArrayList<>();
                for(DataSnapshot child : dataSnapshot.child("students").getChildren())
                {
                    courseStudentUsernames.add(child.getValue(String.class));
                }
                FirebaseData.studentsRef.addListenerForSingleValueEvent(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        Map<String, User> students = new TreeMap<>();
                        for(DataSnapshot child : dataSnapshot.getChildren())
                        {
                            User student = child.getValue(User.class);
                            students.put(student.getUsername(), student);
                        }
                        List<User> nonCourseStudents = new ArrayList<>(students.values());
                        for(String username : courseStudentUsernames)
                        {
                            nonCourseStudents.remove(students.get(username));
                        }
                        onSuccess.run(nonCourseStudents);
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError)
                    {
                        onError.run(firebaseError.getMessage());
                    }
                });
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void removeStudentFromCourse(String courseCode, final String username, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getCourseStudentsRef(courseCode).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    if(child.getValue(String.class).equals(username))
                    {
                        child.getRef().removeValue();
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void getTeacherCourses(final ListRunnable <Course> onSuccess, final SingleArgRunnable<FirebaseError> onError)
    {
        FirebaseData.coursesRef.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                Set<Course> teacherCourses = new TreeSet<>();
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    Course course = child.child("info").getValue(Course.class);
                    if(course.getTeacher().equals(LoginData.getUsername()))
                    {
                        teacherCourses.add(course);
                    }
                }
                onSuccess.run(new ArrayList<>(teacherCourses));
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError);
            }
        });
    }

    public static void updateTeacherUsername(final String oldUsername, final String newUsername)
    {
        FirebaseData.coursesRef.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                for(DataSnapshot courseSnapshot : dataSnapshot.getChildren())
                {
                    Course course = courseSnapshot.child("info").getValue(Course.class);
                    if(course.getTeacher().equals(oldUsername))
                    {
                        course.setTeacher(newUsername);
                    }
                    courseSnapshot.child("info").getRef().setValue(course);
                    for(DataSnapshot noteSnapshot : courseSnapshot.child("revision").child("notes").getChildren())
                    {
                        Note note = noteSnapshot.getValue(Note.class);
                        if(note.getAuthor().equals(oldUsername))
                        {
                            note.setAuthor(newUsername);
                        }
                        noteSnapshot.getRef().setValue(note);
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {

            }
        });
    }

    public static void updateStudentUsername(final String oldUsername, final String newUsername)
    {
        FirebaseData.coursesRef.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                for(DataSnapshot courseSnapshot : dataSnapshot.getChildren())
                {
                    for(DataSnapshot studentSnapshot : courseSnapshot.child("info").child("students").getChildren())
                    {
                        if(studentSnapshot.getValue(String.class).equals(oldUsername))
                        {
                            studentSnapshot.getRef().setValue(newUsername);
                            break;
                        }
                    }
                    for(DataSnapshot questionSnapshot : courseSnapshot.child("classroom").child("questions").getChildren())
                    {
                        for(DataSnapshot responseSnapshot : questionSnapshot.child("responses").getChildren())
                        {
                            Response response = responseSnapshot.getValue(Response.class);
                            if(response.getStudent().equals(oldUsername))
                            {
                                response.setStudent(newUsername);
                                responseSnapshot.getRef().removeValue();
                                questionSnapshot.child("responses").child(newUsername).getRef().setValue(response);
                                break;
                            }
                        }
                    }
                    for(DataSnapshot noteSnapshot : courseSnapshot.child("revision").child("notes").getChildren())
                    {
                        Note note = noteSnapshot.getValue(Note.class);
                        if(note.getAuthor().equals(oldUsername))
                        {
                            note.setAuthor(newUsername);
                            noteSnapshot.getRef().setValue(note);
                        }
                    }
                    for(DataSnapshot perfSnapshot : courseSnapshot.child("performances").getChildren())
                    {
                        if(perfSnapshot.getKey().equals(oldUsername))
                        {
                            Performance performance = perfSnapshot.getValue(Performance.class);
                            perfSnapshot.getRef().removeValue();
                            courseSnapshot.child("performances").child(newUsername).getRef().setValue(performance);
                            break;
                        }
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {

            }
        });
    }

}