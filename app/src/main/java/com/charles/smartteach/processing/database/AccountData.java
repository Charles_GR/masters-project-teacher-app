package com.charles.smartteach.processing.database;

import com.charles.smartteach.model.runnable.SingleArgRunnable;
import com.charles.smartteach.model.database.students.User;
import com.firebase.client.*;
import com.firebase.client.Firebase.ResultHandler;

public abstract class AccountData
{

    public static void changeUsernameAndEmail(final String username, final String currentEmail, final String password, final String newEmail,
                                              final Runnable onSuccess, final SingleArgRunnable<FirebaseError> onError)
    {
        FirebaseData.usersRef.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                boolean usernameExists = dataSnapshot.child("teachers").hasChild(username) || dataSnapshot.child("students").hasChild(username);
                if(usernameExists && !username.equals(LoginData.getUsername()))
                {
                    onError.run(null);
                    return;
                }
                FirebaseData.baseRef.changeEmail(currentEmail, password, newEmail, new ResultHandler()
                {
                    @Override
                    public void onSuccess()
                    {
                        updateUsernameEmailLinks(username, newEmail, onSuccess, onError);
                    }

                    @Override
                    public void onError(FirebaseError firebaseError)
                    {
                        onError.run(firebaseError);
                    }
                });
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError);
            }
        });
    }

    public static void updateUsernameEmailLinks(final String username, final String email, final Runnable onSuccess, final SingleArgRunnable<FirebaseError> onError)
    {
        FirebaseData.getTeacherRef(LoginData.getUsername()).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                User user = dataSnapshot.getValue(User.class);
                CourseData.updateTeacherUsername(LoginData.getUsername(), username);
                FirebaseData.getTeacherRef(LoginData.getUsername()).removeValue();
                LoginData.setUsername(username);
                LoginData.setEmail(email);
                user.setUsername(LoginData.getUsername());
                user.setEmail(LoginData.getEmail());
                FirebaseData.getTeacherRef(LoginData.getUsername()).setValue(user);
                onSuccess.run();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError);
            }
        });
    }

    public static void changePassword(final String email, final String currentPassword, final String newPassword,
                                      final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.baseRef.changePassword(email, currentPassword, newPassword, new ResultHandler()
        {
            @Override
            public void onSuccess()
            {
                onSuccess.run();
            }

            @Override
            public void onError(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

}
