package com.charles.smartteach.processing.database;

import com.charles.smartteach.R;
import com.charles.smartteach.model.*;
import com.charles.smartteach.model.database.students.User;
import com.charles.smartteach.model.runnable.*;
import com.firebase.client.*;
import com.firebase.client.Firebase.ResultHandler;
import com.firebase.client.Firebase.ValueResultHandler;
import java.util.*;

public abstract class StudentData
{

    public static void getStudents(final ListRunnable<User> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.studentsRef.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                List<User> students = new ArrayList<>();
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    students.add(child.getValue(User.class));
                }
                onSuccess.run(students);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void addStudent(final String firstName, final String lastName, final String username, final String email,
                                  final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.usersRef.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                for(DataSnapshot userTypeChild : dataSnapshot.getChildren())
                {
                    for(DataSnapshot userChild : userTypeChild.getChildren())
                    {
                        if(usersEqual(username, email, userChild.getValue(User.class), onError))
                        {
                            return;
                        }
                    }
                }
                FirebaseData.baseRef.createUser(email, "password", new ValueResultHandler<Map<String, Object>>()
                {
                    @Override
                    public void onSuccess(Map<String, Object> result)
                    {
                        FirebaseData.getStudentRef(username).setValue(new User(firstName, lastName, username, email));
                        sendStudentPassword(email, onSuccess, onError);
                    }

                    @Override
                    public void onError(FirebaseError firebaseError)
                    {
                        onError.run(firebaseError.getMessage());
                    }
                });
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void editStudent(final String newFirstName, final String newLastName, final String currentUsername, final String newUsername,
                                   final Runnable onSuccess, final SingleArgRunnable<FirebaseError> onError)
    {
        FirebaseData.usersRef.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                boolean usernameExists = dataSnapshot.child("students").hasChild(newUsername) || dataSnapshot.child("teachers").hasChild(newUsername);
                if(usernameExists && !newUsername.equals(currentUsername))
                {
                    onError.run(null);
                    return;
                }
                FirebaseData.getStudentRef(currentUsername).addListenerForSingleValueEvent(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        User user = dataSnapshot.getValue(User.class);
                        CourseData.updateStudentUsername(currentUsername, newUsername);
                        FirebaseData.getStudentRef(currentUsername).removeValue();
                        user.setFirstName(newFirstName);
                        user.setLastName(newLastName);
                        user.setUsername(newUsername);
                        FirebaseData.getStudentRef(newUsername).setValue(user);
                        onSuccess.run();
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError)
                    {
                        onError.run(firebaseError);
                    }
                });
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError);
            }
        });
    }

    private static void sendStudentPassword(String email, final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.baseRef.resetPassword(email, new ResultHandler()
        {
            @Override
            public void onSuccess()
            {
                onSuccess.run();
            }

            @Override
            public void onError(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    private static boolean usersEqual(String username, String email, User user, SingleArgRunnable<String> onError)
    {
        if(username.equals(user.getUsername()))
        {
            onError.run(TeachApplication.getInstance().getString(R.string.username_in_use));
            return true;
        }
        if(email.equals(user.getEmail()))
        {
            onError.run(TeachApplication.getInstance().getString(R.string.email_in_use));
            return true;
        }
        return false;
    }

}