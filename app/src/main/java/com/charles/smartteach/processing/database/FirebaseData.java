package com.charles.smartteach.processing.database;

import com.charles.smartteach.model.TeachApplication;
import com.firebase.client.Firebase;

public abstract class FirebaseData
{

    public static final Firebase baseRef = new Firebase("https://smart-apps.firebaseio.com/");
    public static final Firebase usersRef = baseRef.child("users");
    public static final Firebase teachersRef = usersRef.child("teachers");
    public static final Firebase studentsRef = usersRef.child("students");
    public static final Firebase coursesRef = baseRef.child("courses");

    public static Firebase getTeacherRef(String username)
    {
        return teachersRef.child(username);
    }

    public static Firebase getStudentRef(String username)
    {
        return studentsRef.child(username);
    }

    public static Firebase getCourseRef(String courseCode)
    {
        return coursesRef.child(courseCode);
    }

    public static Firebase getCourseInfoRef(String courseCode)
    {
        return getCourseRef(courseCode).child("info");
    }

    public static Firebase getCourseStudentsRef(String courseCode)
    {
        return getCourseInfoRef(courseCode).child("students");
    }

    public static Firebase getClassroomQuestionsRef()
    {
        return getCourseRef(TeachApplication.getInstance().getSelectedCourse().toString()).child("classroom").child("questions");
    }

    public static Firebase getClassroomQuestionRef(String key)
    {
        return getClassroomQuestionsRef().child(key);
    }

    public static Firebase getClassroomQuestionResponsesRef(String key)
    {
        return getClassroomQuestionRef(key).child("responses");
    }

    public static Firebase getClassroomCommentsRef()
    {
        return getCourseRef(TeachApplication.getInstance().getSelectedCourse().toString()).child("classroom").child("comments");
    }

    public static Firebase getClassroomCommentRepliesRef(String key)
    {
        return getClassroomCommentsRef().child(key).child("replies");
    }

    public static Firebase getClassroomCommentReplyRef(String commentKey, String replyKey)
    {
        return getClassroomCommentRepliesRef(commentKey).child(replyKey);
    }

    public static Firebase getClassroomImagesRef()
    {
        return getCourseRef(TeachApplication.getInstance().getSelectedCourse().toString()).child("classroom").child("images");
    }

    public static Firebase getClassroomImageRef(String key)
    {
        return getClassroomImagesRef().child(key);
    }

    public static Firebase getRevisionRef()
    {
        return getCourseRef(TeachApplication.getInstance().getSelectedCourse().toString()).child("revision");
    }

    public static Firebase getRevisionNotesRef()
    {
        return getRevisionRef().child("notes");
    }

    public static Firebase getRevisionNoteRef(String key)
    {
        return getRevisionNotesRef().child(key);
    }

    public static Firebase getRevisionNoteCommentsRef(String key)
    {
        return getRevisionNoteRef(key).child("comments");
    }

    public static Firebase getRevisionImagesRef()
    {
        return getRevisionRef().child("images");
    }

    public static Firebase getRevisionQuizzesRef()
    {
        return getRevisionRef().child("quizzes");
    }

    public static Firebase getPerformancesRef(String courseCode)
    {
        return getCourseRef(courseCode).child("performances");
    }

}
